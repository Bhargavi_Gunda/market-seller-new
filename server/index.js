import express from "express";
import proc from "ptmproc";
import indexController from "./controllers/index";

const PORT = 3088;

// initialize the application and create the routes
const app = express();
app.use(indexController);
proc.init(app);

// start the AppContainer
app.listen(PORT, (error) => {
  if (error) {
    return console.log("something bad happened", error);
  }

  console.log("listening on " + PORT + "...");
});

// Handle the bugs somehow
app.on('error', error => {
  console.log(error, 'error in error callback')
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof PORT === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});
