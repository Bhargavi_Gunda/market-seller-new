### Please update port number on line 24 rest will be managed by DevOps.

ARG NODE_VERSION
FROM node:$NODE_VERSION AS builder
WORKDIR /root/.ssh
COPY id_rsa id_rsa
RUN echo "StrictHostKeyChecking no" > config
WORKDIR /var/www/app
COPY . .
ARG BUILD_COMMAND
ENV BUILD_COMMAND=$BUILD_COMMAND
ARG PACKAGE_MANAGER
ENV PACKAGE_MANAGER=$PACKAGE_MANAGER
RUN $PACKAGE_MANAGER install \
   && $PACKAGE_MANAGER $BUILD_COMMAND \
   && rm -rf id_rsa
ARG SYNC_STATIC_FILES
RUN if [ "$SYNC_STATIC_FILES" = "True" ] ; then apt update && apt install awscli -y && bash tools/aws.sh ; fi

FROM node:$NODE_VERSION-alpine
USER node
WORKDIR /var/www/app
COPY  --from=builder /var/www/app .
EXPOSE 8993
CMD node build/server.js