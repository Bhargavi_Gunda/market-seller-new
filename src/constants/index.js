import _urlEndPoints from "./urlEndPoints";
import _actionTypes from "./actionTypes";

export const urlEndPoints = _urlEndPoints;
export const actionTypes = _actionTypes;
