const envBasedDomains = {
  development: {
    host: "https://seller-dev.paytm.com",
    oms: "https://oms-staging.paytm.com",
    fulfillment: "https://fulfillment-staging.paytm.com",
  },
  dev1: {
    host: "https://seller-dev.paytm.com",
    oms: "https://oms-staging.paytm.com",
    fulfillment: "https://fulfillment-staging.paytm.com",
  },
  production: {
    host: "https://seller.paytm.com",
    oms: "https://oms.paytm.com",
    fulfillment: "https://fulfillment.paytm.com",
  },
};

export default {
  ...envBasedDomains.dev1,
  ...envBasedDomains[process.env.NODE_ENV],
};
