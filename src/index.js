import React from "react";
import ReactDOM from "react-dom";
// import Loadable from "react-loadable";
import { Provider as ReduxProvider } from "react-redux";
import App from "./AppContainer/AppContainer";
import configureStore  from "./store/configureStore";


const { store, history } = configureStore(window.__REDUX_STATE__ || {});


const AppBundle = (
  <ReduxProvider store={store}>
       <App />
  </ReduxProvider>
);



window.onload = () => {
  (async function () {
    const { Hawkeye } = window;
    const env = ['development', 'dev1', 'staging', 'production'].includes(process.env.NODE_ENV)?process.env.NODE_ENV:'development';
      Hawkeye &&
      Hawkeye.configure({
        // env - development, staging, production
        env,
        verticalName: "Paytmmall_Seller",
      });
  })();
  const root = document.querySelector('#root');
  if (root.hasChildNodes() === true) {
    // Loadable.preloadReady().then(() => {
      ReactDOM.hydrate(AppBundle, root);
    // });
  } else {
    ReactDOM.render(AppBundle, root);
  }
};
