import axios from 'axios';

export  async function  fetchUserInfo() {
  let p1 =  await axios.get('https://seller-dev.paytm.com/user')
  let p2 = await axios.get('https://seller-dev.paytm.com/leftpanelData')
  return await axios.all([p1, p2])
}