import filter from 'lodash/filter';
import subMenu from '../../config/subMenuConfig';

export const hasPermission = (data, permission) => {
  if (data.some(val => permission.includes(val))) {
    return true;
  } else if (data.every(val => val === 'default')) {
    return true;
  }
  return false;
};

export const checkPermissionLeftPanelInfo = (user1, data) => {
  let user = JSON.parse(user1.success.acl).user;

  const permission = user.permissions || [];
  const filteredTabs = filter(data, tab => {
    if (subMenu.hasOwnProperty(tab.name)) {
      tab.subVerticals = subMenu[tab.name];
    }
    if (tab.isActive && tab.permissions) {
      if (hasPermission(tab.permissions, permission)) {
        if (tab.subVerticals) {
          tab.verticalStatus = false;
          tab.subVerticals = filter(tab.subVerticals, subTab =>
            hasPermission(subTab.permissions, permission),
          );
        }
        return tab;
      }
    }
  });
  return filteredTabs;
};
