/**
 * Created by akshaybindal on 07/04/17.
 */

import _ from 'lodash';

import qs from 'qs';

// import agent from '../../constants/agent';

export const clients = {
  highend: 'HTML5',
  web: 'WEB',
};

/**
 * @description to set value in local storage
 * @param key
 * @param value
 * @param format defaults to json
 */
export const setValueInLocalStorage = (key, value, format = 'json') => {
  try {
    localStorage.setItem(
      key,
      format === 'json' ? JSON.stringify(value) : value,
    );
  } catch (e) {
    console.error(e);
  }
};

export function getParameterByName(names, urls) {
  let url = urls;
  let name = names;
  if (!url) {
    url = window.location.href;
  }
  name = name.replace(/[[\]]/g, '\\$&');
  const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
  const results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export const getSelectedLevelIndexForField = (field, groupIndex, group_arr) => {
  if (
    _.isUndefined(field) ||
    _.isUndefined(groupIndex) ||
    _.isUndefined(group_arr)
  ) {
    return;
  }
  let prevLength = 0;
  const fieldIndexInGroup = _.invoke(group_arr[groupIndex], 'indexOf', field);
  if (fieldIndexInGroup === 0 && Number(groupIndex) === 0) {
    return 0;
  }
  for (let i = 0; i < groupIndex; i++) {
    prevLength += group_arr[i].length;
  }
  return prevLength + fieldIndexInGroup + 1;
};

export function getCookie(name) {
  const re = new RegExp(`${name}=([^;]+)`);
  const value = re.exec(document.cookie);
  return value != null ? unescape(value[1]) : null;
}

export const disableBodyScrolling = condition => {
  if (condition) {
    document.body.style.overflow = 'hidden';
    document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
    // document.getElementsByTagName('html')[0].style.paddingRight =
    //   typeof InstallTrigger !== 'undefined' ? '10px' : '6px';
  } else {
    document.body.style.overflow = 'visible';
    document.getElementsByTagName('html')[0].style.overflowY = 'scroll';
    // document.getElementsByTagName('html')[0].style.paddingRight = '0px';
  }
  // if (agent.isUMP) {
  //   document.getElementsByTagName('html')[0].style.overflowY = '';
  //   document.getElementsByTagName('html')[0].style.overflowX = '';
  // }
};

export const addExtraParams = url =>
  addParamToUrl(url, {
    channel: `${process.env.MOBILE ? clients.highend : clients.web}`,
    child_site_id: 1,
    site_id: 1,
    version: 2,
  });

/* Returns query string with object's key values pairs as
 ?key1=value1&key2=value2 and so-on */
export function stringifyQueryParams(obj) {
  let queryString = '';
  // To-Do  Please remove filter and map chaining , It can be done with
  // single iteration .
  Object.keys(obj)
    .filter(key => !!obj[key] !== false)
    .map(key => (queryString += `${key}=${obj[key]}&`));
  return queryString.length
    ? `?${queryString.substr(0, queryString.length - 1)}`
    : '';
}

export const addParamToUrl = (relativeUrl, queryParam) => {
  const kvp = relativeUrl.split('?');
  let existing = {};
  if (kvp.length > 1) {
    existing = qs.parse(kvp[1]);
  }
  existing = { ...existing, ...queryParam };
  return `${kvp[0]}${stringifyQueryParams(existing)}`;
};

export const showBbpsLogoUrl = (val, arr) => {
  const item = _.find(arr, { filter_name: val });
  return _.get(item, 'attributes.bbps_biller', false)
    ? _.get(item, 'attributes.bbps_logo_url', '')
    : '';
};

export const isLastInAnyGroup = (group_arr, field) => {
  let isLast = false;
  _.forEach(group_arr, (arr) => {
    if (arr.indexOf(field) === arr.length - 1) {
      isLast = true;
      return false;
    }
  });
  return isLast;
};

export const getFieldInfoFromGroupArr = (field, group_arr) => {
  if (_.isUndefined(field) || _.isUndefined(group_arr)) {
    return;
  }
  let info = {};
  _.forEach(group_arr, (arr, i) => {
    const filterIndexInGrp = arr.indexOf(field);
    if (filterIndexInGrp !== -1) {
      info = {
        filterIndexInGrp,
        grpIndex: i,
      };
    }
    return false;
  });
  return info;
};

export const getOpPlanPageTitle = (
  operator,
  plan,
  utility,
  rechargeType,
  rechargeSuffix,
) => {
  if (utility === 'mobile' && rechargeType === 'prepaid') {
    return `${operator} ${plan} Plans - ${operator} ${plan} Recharge at Paytm.com`;
  } else if (utility === 'dth' && rechargeType === 'prepaid') {
    return `${operator} Recharge Plans - ${operator} ${plan} Packages, HD Plans at Paytm`;
  } else if (utility === 'datacard' && rechargeType === 'prepaid') {
    return `${operator} ${plan} Data Card Online Recharge @Paytm`;
  }
  return `${operator} ${plan} ${utility} ${rechargeSuffix} @ Paytm.com`;
};

export const getOpPlanPageDescription = (
  operator,
  plan,
  utility,
  rechargeType,
  rechargeSuffix,
) => {
  if (utility === 'mobile' && rechargeType === 'prepaid') {
    return `${operator} ${plan} Plans - Get the best ${plan} plans to recharge your ${operator} prepaid mobile online at Paytm. Select your city name to know more about tariffs and Offers.`;
  } else if (utility === 'dth' && rechargeType === 'prepaid') {
    return `Get all ${operator} Recharge Plans, Channel Packages & HD Packs at Paytm.com. Use your Credit Card, Debit Card or Net banking for instant ${operator} Recharge Plans. ✓ Fast & Secure.`;
  } else if (utility === 'datacard' && rechargeType === 'prepaid') {
    return `${operator} ${plan} Data Card Recharge – Paytm Offers Online prepaid Datacard recharge for ${plan} ${operator} users through Credit Card, Debit Card or Net banking. ✓ Fast & Secure`;
  }
  return `${operator} ${plan} ${utility} ${rechargeSuffix} @ Paytm.com`;
};

export const getTabNameToShow = (tabName, items) => {
  if (_.find(items, { name: tabName })) {
    return tabName;
  }
  const item = _.get(items, '[0]', {});
  _.set(item, 'selected', true);
  return _.get(item, 'name', '');
};

export const getSummaryDisplayStr = (selectedLevels, group_arr) => {
  const displayList = [];
  _.forEach(group_arr, (arr, i) => {
    _.forEach(arr, (field) => {
      const val = _.get(
        selectedLevels,
        `[${getSelectedLevelIndexForField(field, i, group_arr)}]`,
        '',
      );
      if (val !== 'N/A') {
        displayList.push(val);
      }
    });
  });
  return displayList.join(', ');
};

export const extractServiceOptionData = actions => {
  let displayMsg = '';
  let warningMsg = '';
  let serviceOptData = {};

  _.forEach(actions, (action) => {
    const label = _.get(action, 'label', '');
    if (label === 'display') {
      displayMsg = _.get(action, 'message', '');
    } else if (label === 'warning') {
      warningMsg = _.get(action, 'message', '');
    } else {
      serviceOptData = action;
    }
  });

  return { displayMsg, warningMsg, serviceOptData };
};

export const sanitizeForUrl = str => {
  if (str === undefined) {
    str = '';
  }
  const newStr = _.invoke(str, 'toLowerCase')
    .replace(/[^A-Za-z0-9 ]/g, ' ')
    .replace(/\s/g, '-')
    .replace(/[-]{2,}/g, '-');
  if (newStr.slice(-1) === '-') {
    return newStr.slice(0, -1);
  }
  return newStr;
};

export const formatDthPlanInfo = dthPlanInfo => {
  const services = _.get(dthPlanInfo, 'services', []);
  dthPlanInfo.services = _.map(services, service => {
    service.planExpiryDetails = _.groupBy(
      service.planExpiryDetails,
      'packagename',
    );
    return service;
  });
};
