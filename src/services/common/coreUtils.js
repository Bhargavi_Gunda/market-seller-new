import * as R from 'ramda';
import _ from 'lodash';
import { FORCE_LOGOUT_ROUTE } from '../../constants/constants';

export const urlMapper = item => {
  let itemUrl = '';
  if (
    item &&
    item.seourl &&
    (item.seourl.indexOf('http://') === 0 ||
      item.seourl.indexOf('https://') === 0)
  ) {
    if (item.url_type === 'embed' || item.url_type === 'external') {
      itemUrl = item.url || item.seourl;
    } else if (item.url_type && item.url_type.toLowerCase() === 'nolink') {
      itemUrl = item.seourl.replace(`https://catalog.paytm.com/v1/`, '/');
    } else {
      itemUrl = item.seourl.replace(`https://catalog.paytm.com/v1/`, '/shop/');
    }
  } else if (item.url_type === 'nolink') {
    return '';
  } else {
    itemUrl = `/shop/${_.get(item, 'seourl', '')}`;
  }

  if (itemUrl.indexOf('http://') < 0 && itemUrl.indexOf('https://') < 0) {
    return `http://fe.paytm.com${itemUrl}`;
  }

  return `${itemUrl}`;
};

export const urlTypeToTarget = item => {
  switch (item.url_type) {
    case 'external':
    case 'embed':
      return '_blank';
    default:
      return null;
  }
};

/* Returns query string with object's key values pairs as
 ?key1=value1&key2=value2 and so-on */
export const stringifyQueryParams = obj => {
  let queryString = '';
  // To-Do  Please remove filter and map chaining , It can be done with
  // single iteration .
  obj &&
    Object.keys(obj)
      .filter(key => !!obj[key] !== false)
      .map(key => {
        queryString += `${key}=${obj[key]}&`;
        return queryString;
      });
  return queryString.length
    ? `?${queryString.substr(0, queryString.length - 1)}`
    : '';
};

export const getCookie = name => {
  const re = new RegExp(`${name}=([^;]+)`);
  const value = re.exec(document.cookie);
  return value != null ? unescape(value[1]) : null;
};

export const parseJSON = data =>
  JSON.parse(data, (key, value) => {
    if (
      value &&
      typeof value === 'string' &&
      value.substr(0, 8) == 'function'
    ) {
      const startBody = value.indexOf('{') + 1;
      const endBody = value.lastIndexOf('}');
      const startArgs = value.indexOf('(') + 1;
      const endArgs = value.indexOf(')');
      return new Function(
        value.substring(startArgs, endArgs),
        value.substring(startBody, endBody),
      );
    } else if (
      value &&
      typeof value === 'string' &&
      value.substr(0, 1) == '{' &&
      value.substr(value.length - 1, 1) == '}'
    ) {
      return JSON.parse(value);
    }
    return value;
  });

export function getValueFromQuery(url, key) {
  const obj = url.split('?');
  const list = obj[1].split('&');
  const reg = new RegExp(`^${key}=`);
  let val = '';
  _.forEach(list, item => {
    if (reg.test(item)) {
      val = item.split('=')[1];
      return false;
    }
  });
  return val;
}

export function mapUrlWithValues(url, query) {
  _.forEach(query, (value, key) => {
    url = url.replace(`{${key}}`, value);
  });
  return url;
}

export const logOutUser = () => {
    window.location.href = FORCE_LOGOUT_ROUTE;
  }


export const getLastMondayDate = () =>
  moment()
    .startOf('isoWeek')
    .toDate();

export const getMonthStartDate = () =>
  moment()
    .startOf('month')
    .toDate();

export const getTodayDate = () =>
  moment()
    .startOf('day')
    .toDate();

export const getTodayEndDate = () =>
  moment()
    .endOf('day')
    .toDate();

export const getWeekStartEndDate = () => ({
  startDate: getLastMondayDate().valueOf(),
  endDate: getTodayDate().valueOf(),
});

export const getShortMonthDateString = date => moment(date).format('D MMM');

export const getReportsFilterList = () => [
  {
    label: 'Today',
    data: [getTodayDate(), getTodayDate()],
    selected: false,
    id: 'today',
  },
  {
    label: 'Yesterday',
    data: [
      moment()
        .startOf('day')
        .subtract(1, 'days')
        .toDate(),
      moment()
        .startOf('day')
        .subtract(1, 'days')
        .toDate(),
    ],
    selected: false,
    id: 'yesterday',
  },
  {
    label: 'This week',
    data: [getLastMondayDate(), getTodayDate()],
    selected: false,
    id: 'thisWeek',
  },
  {
    label: 'This month',
    data: [getMonthStartDate(), getTodayDate()],
    selected: false,
    id: 'thisMonth',
  },
  { label: 'Custom Range', data: [], selected: false, id: 'customRange' },
];

export const getCalenderEndDate = (startDate, endDate) =>
  // Maximum date range will be 30 Days
  moment(endDate).isAfter(moment(startDate).add(30, 'days'))
    ? moment(startDate)
        .add(30, 'days')
        .toDate()
    : endDate;

export const showDate = val => {
  if (val.id !== 'customRange') {
    if (val.id === 'today' || val.id === 'yesterday') {
      return `, ${getShortMonthDateString(val.data[0])}`;
    }
    return `, ${getShortMonthDateString(
      val.data[0],
    )} to ${getShortMonthDateString(val.data[1])}`;
  }
  return '';
};

export const removeDuplicates = (myArr, prop = 'id') => {
  const hashMap = {};
  myArr.forEach(mapObj => {
    const id = mapObj[prop];
    if (id) hashMap[id] = mapObj;
  });
  return R.toPairs(hashMap).map(elem => elem[1]);
};
export const uniqueSortByDate = (arr, sortKey = 'updated_at') => {
  const finalArray = removeDuplicates(arr);
  finalArray.sort((a, b) => {
    const keyA = new Date(a[sortKey]);
    const keyB = new Date(b[sortKey]);
    // Compare the 2 dates
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  });

  return finalArray;
};

export const getApiUrlQueryParamsFromURL = (req = {}) => {
  const url = req.url || '';
  const apiUrl = url.split('?')[0];
  const queryParamsString = url.split('?')[1];
  const queryObj = new URLSearchParams(queryParamsString);
  const queryParams = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const pair of queryObj.entries()) {
    queryParams[pair[0]] = pair[1];
  }
  const bodyObj = jsonParse(req.options.body) || {};
  const requestId = req.requestId || '';
  return { apiUrl, queryParams, requestId, body: bodyObj };
};
