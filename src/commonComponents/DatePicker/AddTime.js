/* eslint-disable no-restricted-globals */
import React, { Component } from "react";

import Input from "./Input/Input";
import s from "./DatePicker.scss";
import { checkTimeDiffError } from "./utils";

class AddTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time:
        props.type === "From"
          ? props.rangeObj.startTime || {
              hours: "00",
              minutes: "00",
              meridiem: "AM",
            }
          : props.rangeObj.endTime || {
              hours: "11",
              minutes: "59",
              meridiem: "PM",
            },
      isError: !!props.hasTimeError,
    };
  }

  componentDidMount() {
    const { type, rangeObj } = this.props;
    const { time } = this.state;
    if (time) {
      const isError = checkTimeDiffError(type, rangeObj, time);
      /* eslint-disable-next-line react/no-did-mount-set-state */
      this.setState({
        isError,
      });
      this.props.handleTimeChange(time, type, isError);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.hasTimeError !== this.props.hasTimeError) {
      /* eslint-disable-next-line react/no-did-update-set-state */
      this.setState({ isError: this.props.hasTimeError });
    }
  }

  onInputChange = (event) => {
    const { type, rangeObj } = this.props;
    const { name, value } = event.target;
    const { time, isError: error } = this.state;
    let isError = error;
    const newValue = name === "meridiem" && value ? value.toUpperCase() : value;
    if (this.isValidTimeInput(name, newValue)) {
      time[name] = newValue;
      if (newValue.length === 2) {
        isError = checkTimeDiffError(type, rangeObj, time);
        this.props.handleTimeChange(time, type, isError);
      }
    }
    return this.setState({ time, isError });
  };

  isValidTimeInput = (name, value) => {
    if (value) {
      if (name === "hours") {
        return !(isNaN(value) || value > 12);
      } else if (name === "minutes") {
        return !(isNaN(value) || value > 59);
      } else if (name === "meridiem") {
        return !!(
          (value.length === 1 && ["A", "P"].includes(value[0])) ||
          (value.length === 2 &&
            ["A", "P"].includes(value[0]) &&
            value[1] === "M")
        );
      }
    } else {
      return true;
    }
    return false;
  };

  onBlur = (event) => {
    const { type, rangeObj } = this.props;
    const { name, value } = event.target;
    const { time } = this.state;
    if (name === "hours") {
      if (!value) {
        time[name] = type === "From" ? "00" : "11";
      } else if (value.length === 1) {
        time[name] = `0${value}`;
      }
    } else if (name === "minutes") {
      if (!value) {
        time[name] = type === "From" ? "00" : "59";
      } else if (value.length === 1) {
        time[name] = `0${value}`;
      }
    } else if (name === "meridiem") {
      if (!value) {
        time[name] = type === "From" ? "AM" : "PM";
      } else if (value.length === 1) {
        time[name] = `${value}M`;
      }
    }
    const isError = checkTimeDiffError(type, rangeObj, time);
    this.setState({ time, isError });
    this.props.handleTimeChange(time, type, isError);
  };

  render() {
    const { time, isError } = this.state;
    const { rangeObj } = this.props;
    const startDate = JSON.stringify(rangeObj.startDate).split("T")[0];
    const endDate = JSON.stringify(rangeObj.endDate).split("T")[0];
    return time ? (
      <div>
        <div className={s.timeInput}>
          <Input
            type="text"
            name="hours"
            value={time.hours}
            onInput={this.onInputChange}
            required
            maxLength={2}
            placeholder="Hours"
            onBlur={this.onBlur}
            isLabelRight
          />
          <Input
            type="text"
            name="minutes"
            value={time.minutes}
            onInput={this.onInputChange}
            required
            maxLength={2}
            placeholder="Minutes"
            onBlur={this.onBlur}
            isLabelRight
          />
          <Input
            type="text"
            name="meridiem"
            value={time.meridiem}
            onInput={this.onInputChange}
            required
            maxLength={2}
            onBlur={this.onBlur}
            placeholder=""
            isLabelRight
          />
        </div>
        {startDate === endDate && isError && (
          <div className={s.timeError}>
            End time should be greater than start time for same date.
          </div>
        )}
      </div>
    ) : null;
  }
}

export default AddTime;
