import React from "react";
import cx from "../../utils/classNames";

import s from "./style.scss";

const Input = (props) => {
  const {
    placeholder,
    type,
    id,
    name,
    value,
    onFocus,
    onInput,
    required,
    onInvalid,
    onBlur,
    errorMsg,
    maxLength,
    minLength,
    className,
    autocomplete,
  } = props;
  return (
    <div className={s.time}>
      <input
        type={type}
        id={id}
        name={name}
        value={value}
        onInput={onInput}
        required={required}
        maxLength={maxLength}
        minLength={minLength}
        onFocus={onFocus}
        onInvalid={onInvalid}
        onBlur={onBlur}
        autoComplete={autocomplete}
        className={cx({
          [s[className]]: true,
          [s.customFloatInput]: true,
          [s.hasValue]: !!value,
          [s.hasError]: !!errorMsg,
        })}
      />
      <label className={`${s.timeLabel}`}>{placeholder}</label>
      <label className={`${s.customFloatError}`}>{errorMsg}</label>
    </div>
  );
};
export default Input;
