import React, { Component } from "react";

import AddTime from "./AddTime";
import s from "./DatePicker.scss";

const isSelected = (day, currentDate) => {
  day = day.toLocaleDateString().split("-").reverse().join("-");
  currentDate = currentDate.toLocaleDateString().split("-").reverse().join("-");
  return Boolean(day === currentDate);
};
class DatePicker extends Component {
  constructor(props) {
    super(props);
    const {
      minDate,
      maxDate,
      selectedDate,
      showBackward,
      showForward,
      displayedMonth,
      displayedYear,
    } = props;
    this.now = new Date();
    let minDateValue;
    let currentDate = this.now;
    if (minDate) {
      minDateValue = new Date(minDate);
    }

    if (selectedDate) {
      currentDate = new Date(selectedDate);
    }
    this.state = {
      currentDate,
      showBackward: showBackward || true,
      showForward: showForward || true,
      displayedMonth: displayedMonth || currentDate.getMonth(),
      displayedYear: displayedYear || currentDate.getFullYear(),
      minDate: minDateValue || new Date("1800"),
      maxDate: maxDate || new Date("2050"),
    };
    this.monthArrShortFull = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    this.monthArrShort = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];

    this.dayArr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  }

  handleDateChange = (e) => {
    const date = new Date(e.target.value);
    this.setState({
      currentDate: date,
      displayedMonth: date.getMonth() || 1,
      displayedYear: date.getFullYear(),
    });
  };

  getAllDates = (day, selected, key) => {
    const isDisable =
      day.date < this.state.minDate || day.date > this.state.maxDate;
    return (
      <span
        className={`${day.today ? s.datePickerToday : ""} ${
          selected ? s.datePickerSelected : ""
        }`}
        disabled={isDisable}
        date={day.date}
        key={key}
      >
        {day.day}
      </span>
    );
  };
  displayPrevMonth = () => {
    if (this.state.displayedMonth <= 0) {
      this.setState({
        displayedMonth: 11,
        displayedYear: this.state.displayedYear - 1,
      });
    } else {
      this.setState({
        displayedMonth: this.state.displayedMonth - 1,
      });
    }
    if (this.props.onMonthsChanged) {
      this.props.onMonthsChanged(this.state.displayedMonth);
    }
  };

  displayNextMonth = () => {
    if (this.state.displayedMonth >= 11) {
      this.setState({
        displayedMonth: 0,
        displayedYear: this.state.displayedYear + 1,
      });
    } else {
      this.setState({
        displayedMonth: this.state.displayedMonth + 1,
      });
    }

    if (this.props.onMonthsChanged) {
      this.props.onMonthsChanged(this.state.displayedMonth);
    }
  };

  /**
   * Gets fired when a day gets clicked.
   * @param {object} e The event thrown by the <span /> element clicked
   */
  dayClicked = (e) => {
    const element = e.target; // the actual element clicked

    if (element.innerHTML === "") {
      return false;
    } // don't continue if <span /> empty

    // get date from clicked element (gets attached when rendered)
    const date = new Date(element.getAttribute("date"));
    if (date < this.props.minDate || date > this.props.maxDate) {
      return false;
    }
    this.setState({ currentDate: date });

    if (this.props.dateLabel) {
      this.props.updateParentDate({ label: this.props.dateLabel, value: date });
    }
    return true;
  };

  /**
   * returns days in month as array
   * @param {number} month the month to display
   * @param {number} year the year to display
   */
  getDaysByMonth(month, year) {
    const calendar = [];

    const firstDay = new Date(year, month, 1).getDay(); // first weekday of month
    const lastDate = new Date(year, month + 1, 0).getDate(); // last date of month

    let day = 0;

    // the calendar is 7*6 fields big, so 42 loops
    for (let i = 0; i < 42; i++) {
      if (i >= firstDay && day !== null) {
        day += 1;
      }
      if (day > lastDate) {
        day = null;
      }

      // append the calendar Array
      calendar.push({
        day: day === 0 || day === null ? null : day, // null or number
        date: day === 0 || day === null ? null : new Date(year, month, day), // null or Date()
        today:
          day === this.now.getDate() &&
          month === this.now.getMonth() &&
          year === this.now.getFullYear(), // boolean
      });
    }

    return calendar;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.minDate !== this.props.minDate) {
      const { currentDate } = this.state;
      const minDate = nextProps.minDate;
      this.setState({ minDate });
      if (currentDate < minDate) {
        this.setState({ currentDate: minDate });
        this.props.updateParentDate({
          label: this.props.dateLabel,
          value: minDate,
        });
      }
    }
  }

  render() {
    const {
      type,
      rangeObj,
      handleTimeChange,
      hasTimeError,
      showTime,
    } = this.props;
    return (
      <div>
        <div className={s.datePickerWrap}>
          <div className={`${s.pickerWrapper} pickWrapper`}>
            <div className={s.datePicker}>
              {showTime && (
                <AddTime
                  hasTimeError={hasTimeError}
                  rangeObj={rangeObj}
                  type={type}
                  handleTimeChange={handleTimeChange}
                />
              )}
              {showTime ? (
                <nav>
                  {this.state.showBackward && (
                    <span className={s.arrowBox}>
                      <i className={s.left} onClick={this.displayPrevMonth} />
                    </span>
                  )}
                  <h4>
                    {this.monthArrShort[this.state.displayedMonth]}{" "}
                    {this.state.displayedYear}
                  </h4>
                  {this.state.showForward && (
                    <span className={s.arrowBox}>
                      <i className={s.right} onClick={this.displayNextMonth} />
                    </span>
                  )}
                </nav>
              ) : (
                <nav>
                  {this.state.showBackward && (
                    <span className={s.arrowBox}>
                      <i className={s.left} onClick={this.displayPrevMonth} />
                    </span>
                  )}
                  <h4>
                    {this.monthArrShort[this.state.displayedMonth]}{" "}
                    {this.state.displayedYear}
                  </h4>
                  {this.state.showForward && (
                    <span className={s.arrowBox}>
                      <i className={s.right} onClick={this.displayNextMonth} />
                    </span>
                  )}
                </nav>
              )}
              <div className={s.datePickerScroll}>
                <div className={s.datePickerCalendar}>
                  <div className={s.datePickerDayNames}>
                    {this.dayArr.map((day) => (
                      <span key={day}>{day}</span>
                    ))}
                  </div>

                  <div onClick={this.dayClicked} className={s.datePickerDays}>
                    {this.getDaysByMonth(
                      this.state.displayedMonth,
                      this.state.displayedYear
                    ).map((day, index) => {
                      let selected = false;
                      if (this.state.currentDate && day.date) {
                        selected = isSelected(day.date, this.state.currentDate);
                      }
                      return this.getAllDates(day, selected, index);
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DatePicker;
