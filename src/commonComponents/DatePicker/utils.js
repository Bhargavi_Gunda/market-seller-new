export const getTotalTime = (time) => {
  const totalTime =
    time.hours === "12"
      ? parseInt(time.minutes, 10)
      : parseInt(time.hours, 10) * 60 + parseInt(time.minutes, 10);
  return time.meridiem === "PM" ? totalTime + 12 * 60 : totalTime;
};

export const checkTimeDiffError = (type, rangeObj, time) => {
  const startDate = JSON.stringify(rangeObj.startDate).split("T")[0];
  const endDate = JSON.stringify(rangeObj.endDate).split("T")[0];
  let startTotalTime;
  let endTotalTime;
  if (type === "From") {
    startTotalTime = getTotalTime(time);
    endTotalTime = getTotalTime(
      rangeObj.endTime || { hours: "11", minutes: "59", meridiem: "PM" }
    );
  } else if (type === "To") {
    startTotalTime = getTotalTime(
      rangeObj.startTime || { hours: "00", minutes: "00", meridiem: "AM" }
    );
    endTotalTime = getTotalTime(time);
  }
  return !!(startDate === endDate && startTotalTime > endTotalTime);
};
