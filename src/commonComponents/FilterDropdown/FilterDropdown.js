/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react/require-default-props */
import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import cx from "../../utils/classNames";
import _ from "lodash";
import s from "./FilterDropdown.scss";
import { disableBodyScrolling } from "../../services/common/coreUtilsHomePage";

import InfoTooltip from "../../components/Common/InfoTooltip/InfoTooltip";

class Dropdown extends PureComponent {
  static defaultProps = {
    onClick: () => {},
  };
  static propTypes = {
    optionList: PropTypes.instanceOf(Array).isRequired,
    onClick: PropTypes.func,
    selected: PropTypes.instanceOf(Object).isRequired,
    label: PropTypes.string,
  };
  state = {
    optionList: this.props.optionList || [],
    showOptionList: false,
    selectedItem:
      this.props.optionList &&
      _.find(this.props.optionList, this.props.selected) &&
      _.get(this.props.optionList, "0"), // check arraylist and get first object selected,
    disabled: this.props.disabled || false,
  };
  componentDidMount = () => {
    document.documentElement.addEventListener("click", this.clickOutSide);
    this.setState({
      selectedItem: _.find(this.props.optionList, this.props.selected),
    });
  };
  componentWillReceiveProps = (nextProps) => {
    if (this.props.optionList !== nextProps.optionList) {
      this.setState({
        optionList: nextProps.optionList,
      });
    }
    // ============================================
    // @info
    // ============================================
    // update new selected data
    if (this.props.optionList && this.props.selected) {
      if (this.props.selected !== nextProps.selected) {
        this.setState({
          selectedItem: _.find(nextProps.optionList, nextProps.selected),
        });
      }
    }
  };

  componentWillUnmount = () => {
    document.documentElement.removeEventListener("click", this.clickOutSide);
    disableBodyScrolling(false);
  };

  onKeyPressHandler = () => true;

  // fires on initial click: it will open the dropdown list
  handleDropdown = (evt) => {
    !this.state.showOptionList && disableBodyScrolling(true);
    if (this.state.disabled) {
      evt.preventDefault();
      return false;
    }
    this.setState((prevState) => ({
      showOptionList: !prevState.showOptionList,
    }));

    return false;
  };

  // it will fires on outside click  check didmount
  clickOutSide = (evt) => {
    this.state.showOptionList && disableBodyScrolling(false);
    if (this.selectDropdown && !this.selectDropdown.contains(evt.target)) {
      this.setState({
        showOptionList: false,
      });
      if (this.props.onDropdownClose) {
        this.props.onDropdownClose();
      }
    }
  };

  // fires on single item click (option/dropdown list)
  handleSelected(item) {
    this.setState({
      selectedItem: item,
      showOptionList: false,
    });

    if (this.props.onClick) {
      this.props.onClick(item); // get onclick from props and fire it here
    }
    if (this.props.handleFilterClick) {
      this.props.handleFilterClick(item);
    }
  }

  render() {
    const label = this.props.label || "STATUS";
    return (
      <div className={cx(s.dropdown, this.state.showOptionList ? s.open : "")}>
        <div className={s.dLabel}>
          <div className={s.dLabelTitle}>
            <span>{label}</span>
            {this.props.showTooltip ? (
              <InfoTooltip tooltipContent={this.props.tooltipContent} />
            ) : null}
          </div>
          <div
            ref={(input) => {
              this.selectDropdown = input; // based on ref checking if click is inside or outside and allowing toggle from arrow icon
            }}
            className={
              this.state.disabled
                ? cx(s.disabled, s.selectedTxt)
                : s.selectedTxt
            }
            onClick={(evt) => this.handleDropdown(evt)}
          >
            {this.state.selectedItem
              ? _.truncate(
                  this.props.keySelected
                    ? this.state.selectedItem[this.props.keySelected]
                    : this.state.selectedItem.text,
                  {
                    omission: "...",
                    length: 15,
                  }
                )
              : null}
            <i />
          </div>
        </div>
        <ul className={s.dropdownMenu}>
          {_.map(this.state.optionList, (item, k) => (
            <li
              key={item.id || item.key || k}
              onClick={() => this.handleSelected(item)}
              role="presentation"
              onKeyPress={this.onKeyPressHandler}
              className={
                this.state.selectedItem === item ? s.selectedRowItem : ""
              }
            >
              {this.props.keySelected
                ? item[this.props.keySelected]
                : item.text}
            </li>
          ))}
        </ul>
        <div className={s.backdrop} />
      </div>
    );
  }
}

export default Dropdown;
