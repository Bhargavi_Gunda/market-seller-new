import React, { Component } from "react";

import s from "./style.scss";

class Overlay extends Component {
  render() {
    const { showDropdownMethod, show } = this.props;
    return (
      <div
        onClick={showDropdownMethod}
        className={`${s.overlay} ${show ? s.active : ""}`}
      />
    );
  }
}

export default Overlay;
