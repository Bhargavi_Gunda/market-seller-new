/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from "react";
import * as moment from "moment";
import cx from "../../utils/classNames";

import DatePicker from "../DatePicker/DatePicker";
import Overlay from "./Overlay/Overlay";
import s from "./DatePickerFilter.scss";
import { getTotalTime } from "../DatePicker/utils";
import { getCalenderEndDate } from "../../services/common/coreUtils";

const SELECT_BOX_WIDTH = 250; // width in px
const DATE_PICKER_WIDTH = 450; // width in px

class DatePickerFilter extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitState(props);
    this.selectBoxRef = React.createRef();
  }

  getInitState(props) {
    const {
      dateOptions: {
        defaultFilter = "today",
        dataFilterList,
        selectedDate,
        selectedEndDate,
      } = {},
    } = props;
    this.fromPickerMinDate = moment().subtract(12, "months").toDate();
    return {
      dateFilterList: this.getFilterList(defaultFilter, dataFilterList),
      currentPicker: "from",
      isDateRangeVisible: false,
      isDoneButtonDisable: false,
      rangeObj: {
        startDate: selectedDate,
        endDate: selectedEndDate,
        startTime: {
          hours: "00",
          minutes: "00",
          meridiem: "AM",
        },
        endTime: {
          hours: "11",
          minutes: "59",
          meridiem: "PM",
        },
      },
    };
  }
  componentWillReceiveProps(nextProps) {
    if (
      "isDateRangeVisible" in nextProps &&
      nextProps.isDateRangeVisible !== this.props.isDateRangeVisible
    ) {
      this.setState({ isDateRangeVisible: nextProps.isDateRangeVisible });
    }
  }

  getShortMonthDateString = (date) => moment(date).format("D MMM");

  momentFormatDate = (date, reqFormat, dateFormat = "") =>
    dateFormat
      ? moment(date, dateFormat).format(reqFormat)
      : moment(date).format(reqFormat);

  getFilterList = (defaultFilter = "today", list) => {
    const labelList = [
      "today",
      "yesterday",
      "thisWeek",
      "thisMonth",
      "customRange",
    ];
    const index = labelList.indexOf(defaultFilter);
    if (index !== -1) {
      list[index].selected = true;
    }
    return list;
  };

  hasTimeError = (rangeObj) => {
    const startDate = JSON.stringify(rangeObj.startDate).split("T")[0];
    const endDate = JSON.stringify(rangeObj.endDate).split("T")[0];
    const startTotalTime = getTotalTime(
      rangeObj.startTime || { hours: "00", minutes: "00", meridiem: "AM" }
    );
    const endTotalTime = getTotalTime(
      rangeObj.endTime || { hours: "11", minutes: "59", meridiem: "PM" }
    );
    return !!(startDate === endDate && startTotalTime > endTotalTime);
  };

  updateDate = (date) => {
    const { rangeObj } = this.state;
    rangeObj[date.label] = date.value;
    if (date.label === "startDate") {
      if (moment(rangeObj.startDate).isAfter(moment(rangeObj.endDate))) {
        rangeObj.endDate = rangeObj.startDate;
      }
      rangeObj.endDate = moment(rangeObj.startDate).toDate();
      this.setState({ currentPicker: "to" });
    }
    const hasTimeError = this.hasTimeError(rangeObj);
    this.setState({ rangeObj, isDoneButtonDisable: hasTimeError });
  };

  updateByFilter = (val, index) => {
    const { dateFilterList } = this.state;
    if (val.id !== "customRange") {
      const rangeObj = {
        ...this.state.rangeObj,
        startDate: val.data[0],
        endDate: val.data[1],
        startTime: {
          hours: "00",
          minutes: "00",
          meridiem: "AM",
        },
        endTime: {
          hours: "11",
          minutes: "59",
          meridiem: "PM",
        },
      };
      this.props.dateOptions.toggleDatePicker();
      this.props.dateOptions.onDateChange(rangeObj);
      this.props.dateOptions.onUpdateDefaultFilter(dateFilterList[index].id);
      this.setState({ rangeObj });
    } else {
      this.setState({
        isDateRangeVisible: true,
      });
    }

    // Reset
    dateFilterList.forEach((value) => {
      value.selected = false;
    });
    dateFilterList[index].selected = true;
    this.setState({ dateFilterList });
  };

  updateDateForParent = () => {
    const { endDate, startDate } = this.state.rangeObj;
    endDate.setHours(0, 0, 0, 0);
    startDate.setHours(0, 0, 0, 0);
    this.props.dateOptions.toggleDatePicker();
    this.props.dateOptions.onDateChange(this.state.rangeObj);
    this.props.dateOptions.onUpdateDefaultFilter("customRange");
    this.setState({ isDateRangeVisible: false, currentPicker: "from" });
  };

  checkDateOutOfRange = (rangeObj) => {
    const endDate = moment(rangeObj.endDate);
    const startDate = moment(rangeObj.startDate);
    const duration = moment.duration(endDate.diff(startDate));
    const days = duration.asDays();
    let status = false;
    if (days > 30) {
      status = true;
    }
    return status;
  };

  showDate(val) {
    if (val.id !== "customRange") {
      // For Bill Payments short month date string is not required
      if (val.id === "today" || val.id === "yesterday") {
        return `, ${this.getShortMonthDateString(val.data[0])}`;
      }
      return `, ${this.getShortMonthDateString(
        val.data[0]
      )} to ${this.getShortMonthDateString(val.data[1])}`;
    }
    return "";
  }

  getDateFilterList = () =>
    this.state.dateFilterList.map((val, index) => (
      <div
        key={val.label}
        onClick={() => this.updateByFilter(val, index)}
        className={`${s.dataRow} ${val.selected ? s.active : ""}`}
      >
        {val.label}
        {this.showDate(val)}
      </div>
    ));
  getCurrentFilterSelected = (id) => {
    const { dateFilterList } = this.state;
    const filter = dateFilterList.filter((val) => val.id === id);
    return filter[0] ? filter[0] : {};
  };

  cancelDatePicker = (hidePicker = false) => {
    const { toggleDatePicker } = this.props.dateOptions;
    this.setState(this.getInitState(this.props));
    if (hidePicker) {
      toggleDatePicker(false);
    }
  };

  showSelectedDatePlaceholder = () => {
    const {
      dateOptions: { defaultFilter = "today" } = {},
      showTime,
    } = this.props;
    const { rangeObj } = this.state;
    const selectedFilter = this.getCurrentFilterSelected(defaultFilter);
    if (selectedFilter.id === "today" || selectedFilter.id === "yesterday") {
      return `${selectedFilter.label}${this.showDate(selectedFilter)}`;
    }
    const startTimeLabel = `${rangeObj.startTime.hours}:${rangeObj.startTime.minutes} ${rangeObj.startTime.meridiem}`;
    const endTimeLabel = `${rangeObj.endTime.hours}:${rangeObj.endTime.minutes} ${rangeObj.endTime.meridiem}`;
    if (showTime) {
      return `${this.getShortMonthDateString(
        this.state.rangeObj.startDate
      )}, ${startTimeLabel} - ${this.getShortMonthDateString(
        this.state.rangeObj.endDate
      )}, ${endTimeLabel}`;
    }
    return `${selectedFilter.label}, ${this.getShortMonthDateString(
      this.state.rangeObj.startDate
    )} to ${this.getShortMonthDateString(this.state.rangeObj.endDate)}`;
  };

  togglePicker = (currentPicker) => {
    this.setState({ currentPicker });
  };

  handleTimeChange = (time, type, isError) => {
    const { rangeObj } = this.state;
    if (type === "From") {
      rangeObj.startTime = time;
    } else if (type === "To") {
      rangeObj.endTime = time;
    }
    this.setState({ rangeObj, isDoneButtonDisable: isError });
  };

  componentDidUpdate(prevProps) {
    const { dateOptions: { defaultFilter } = {} } = prevProps;
    const {
      dateOptions: { defaultFilter: newDefaultFilter } = {},
    } = this.props;
    if (newDefaultFilter !== defaultFilter) {
      /* eslint-disable-next-line react/no-did-update-set-state */
      this.setState(this.getInitState(this.props));
    }
  }

  getAlignLeftMargin = () => {
    const { align } = this.props;
    if (!align) {
      return 0;
    }
    const { isDateRangeVisible } = this.state;
    const selectBoxWidth = this.selectBoxRef.current.clientWidth;
    return isDateRangeVisible
      ? -(DATE_PICKER_WIDTH - selectBoxWidth)
      : -(SELECT_BOX_WIDTH - selectBoxWidth);
  };

  render() {
    const { dateOptions, label, showTime } = this.props;
    const {
      rangeObj,
      currentPicker,
      isDateRangeVisible,
      isDoneButtonDisable,
    } = this.state;
    let startTimeLabel = "";
    let endTimeLabel = "";
    if (showTime) {
      startTimeLabel = rangeObj.startTime
        ? `${rangeObj.startTime.hours}:${rangeObj.startTime.minutes} ${rangeObj.startTime.meridiem}`
        : "";
      endTimeLabel = rangeObj.endTime
        ? `${rangeObj.endTime.hours}:${rangeObj.endTime.minutes} ${rangeObj.endTime.meridiem}`
        : "";
    }

    // Maximum date range will be 30 Days
    const toMaxDate = getCalenderEndDate(
      rangeObj.startDate,
      dateOptions.maxDate
    );
    return (
      <div
        className={cx({
          [s.hasOverlay]: dateOptions.datePickerOpened,
        })}
      >
        <div
          className={s.selectSearch}
          onClick={() => dateOptions.toggleDatePicker(true)}
          ref={this.selectBoxRef}
        >
          {label && <label>{label}</label>}
          <div className={s.selectBox}>
            <span>
              {this.showSelectedDatePlaceholder()}
              <i
                className={cx({
                  [s.up]: dateOptions.datePickerOpened,
                })}
              />
            </span>
          </div>
        </div>

        {dateOptions.datePickerOpened && (
          <div
            className={cx(s.datePickerWrap, dateOptions.className)}
            style={{ left: this.getAlignLeftMargin() }}
          >
            {isDateRangeVisible && (
              <div>
                <div className={s.datePickerWrap}>
                  <div className={s.customDateBox}>
                    <div className={s.topDateRange}>
                      <i
                        className="arrowBack"
                        onClick={() => this.cancelDatePicker(false, "btn")}
                      />
                      <div className={s.customText}>
                        Custom Date Range
                        <span>Maximum date range will be 31 Days</span>
                      </div>
                      <button
                        className={cx({
                          [s.disableButton]: isDoneButtonDisable,
                        })}
                        onClick={this.updateDateForParent}
                      >
                        Apply
                      </button>
                    </div>
                  </div>
                  <div className={s.actionWrapper}>
                    <div className={s.filterActionBottom}>
                      <div
                        className={`${s.calenderChoose} ${
                          currentPicker === "from" ? s.active : ""
                        }`}
                        onClick={() => this.togglePicker("from")}
                      >
                        <label>Start Date</label>
                        <input
                          type="text"
                          value={`${this.momentFormatDate(
                            rangeObj.startDate,
                            "DD-MM-YYYY"
                          )} ${startTimeLabel}`}
                          readOnly
                        />
                        <i className="today" />
                      </div>
                      <div
                        className={`${s.calenderChoose} ${
                          currentPicker === "to" ? s.active : ""
                        }`}
                        onClick={() => this.togglePicker("to")}
                      >
                        <label>End Date</label>
                        <input
                          type="text"
                          value={`${this.momentFormatDate(
                            rangeObj.endDate,
                            "DD-MM-YYYY"
                          )} ${endTimeLabel}`}
                          readOnly
                        />
                        <i className="today" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className={s.calWrap}>
                  {currentPicker === "from" && (
                    <div
                      className={cx(s.DPconWrapper, s.calenderRight)}
                      id="from"
                    >
                      <DatePicker
                        minDate={this.fromPickerMinDate}
                        maxDate={dateOptions.maxDate}
                        selectedDate={rangeObj.startDate}
                        dateLabel="startDate"
                        updateParentDate={this.updateDate}
                        type="From"
                        key="from"
                        rangeObj={rangeObj}
                        handleTimeChange={this.handleTimeChange}
                        showTime={showTime}
                        hasTimeError={isDoneButtonDisable}
                      />
                    </div>
                  )}
                  {currentPicker === "to" && (
                    <div className={cx(s.DPconWrapper, s.calenderLeft)} id="to">
                      <DatePicker
                        minDate={rangeObj.startDate}
                        maxDate={toMaxDate}
                        selectedDate={rangeObj.endDate}
                        updateParentDate={this.updateDate}
                        dateLabel="endDate"
                        type="To"
                        key="to"
                        rangeObj={rangeObj}
                        handleTimeChange={this.handleTimeChange}
                        showTime={showTime}
                        hasTimeError={isDoneButtonDisable}
                      />
                    </div>
                  )}
                </div>
              </div>
            )}

            <div className={s.dateContainer}>
              <div className={s.dataBox}>{this.getDateFilterList()}</div>
            </div>
          </div>
        )}
        <Overlay
          show={dateOptions.datePickerOpened}
          showDropdownMethod={() => this.cancelDatePicker(true, "overlay")}
        />
      </div>
    );
  }
}

export default DatePickerFilter;
