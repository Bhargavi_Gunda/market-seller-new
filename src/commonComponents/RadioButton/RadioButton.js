import React, { Component } from "react";

import PropTypes, {
  /* tree-shaking no-side-effects-when-called */ arrayOf,
  /* tree-shaking no-side-effects-when-called */ shape,
  /* tree-shaking no-side-effects-when-called */ oneOfType,
} from "prop-types";
import cx from "../../utils/classNames";
import styles from "./RadioButton.module.scss";
import noop from "../../components/CatalogDIY/utils/noop";

class RadioButton extends Component {
  handleChange = (evt) => {
    const { onChange } = this.props;
    if (onChange) {
      const {
        target: { value },
      } = evt;
      onChange(value, evt);
    }
  };

  render() {
    const {
      props: {
        buttons,
        groupName,
        className,
        groupLabel,
        error,
        selected,
        required,
        disabled: disableAll,
      },
    } = this;
    return (
      <React.Fragment>
        <label className={required ? styles.required : ""}>{groupLabel}</label>
        <div
          className={cx(styles.radioBar, className)}
          onChange={this.handleChange}
        >
          {buttons.map((item) => {
            const { id, value, label, disabled } = item;
            const disable = disableAll || disabled || false;
            return (
              <div
                className={cx(styles.radio, {
                  [styles.radioDisabled]: disable,
                })}
                key={`${groupName}_${id}`}
              >
                <input
                  id={id}
                  name={groupName}
                  type="radio"
                  value={value}
                  checked={value === selected}
                  disabled={disable}
                  onChange={noop} // onchange is handled on uper div
                />
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label htmlFor={id} className={styles.radioLabel}>
                  {label}
                </label>
              </div>
            );
          })}
        </div>
        {error ? <div className={styles.Error}>{error}</div> : null}
      </React.Fragment>
    );
  }
}

RadioButton.propTypes = {
  /** your custom class for radio buttons */
  className: PropTypes.string,
  /** array of all the buttons with following structure */
  buttons: arrayOf(
    shape({
      /** id for radioButton */
      id: oneOfType([PropTypes.number, PropTypes.string]).isRequired,
      /** name of the label which is showed right next to the radioButton */
      label: oneOfType([PropTypes.string, PropTypes.node]).isRequired,
      /** value of radioButton */
      value: PropTypes.string.isRequired,
      /** disable that option */
      disabled: PropTypes.bool,
    })
  ).isRequired,
  /**  onChange handler for radioButton */
  onChange: PropTypes.func,
  /** selector/name for radioButton to group it in one */
  groupName: PropTypes.string.isRequired,
};

RadioButton.defaultProps = {
  onChange: null,
  className: "",
};

export default RadioButton;
