export const STEP_VALUES = {
  ONBORDING: 1,
  LOGIN_IFRAME: 2,
  MIGRATION_OTP: 3,
  SUCCESS: 4,
};
export const OTP_CONFIG = {
  length: 6,
  timeLimit: 120,
  otpDefaultValue: ["", "", "", "", "", ""],
};
