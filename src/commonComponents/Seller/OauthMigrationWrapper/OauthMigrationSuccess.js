import React, { Component } from "react";
import PropTypes from "prop-types";

import s from "./OauthMigrationWrapper.scss";
import { logOutUser } from "../../services/common/coreUtils";

class OauthMigrationSuccess extends Component {
  static defaultProps = {
    closeModal: () => {},
    isBlockingPopUp: false,
  };
  static propTypes = {
    closeModal: PropTypes.func,
    isBlockingPopUp: PropTypes.bool,
  };
  closeModal = () => {
    logOutUser();
  };
  render() {
    return (
      <div className={s.succssWrapper}>
        {!this.props.isBlockingPopUp ? (
          <div className={s.closeIcon}>
            <i
              onClick={() => this.props.closeModal()}
              role="presentation"
              className="clear"
            />
          </div>
        ) : (
          ""
        )}
        <div className={s.successIcon}>
          <i className="success">
            <span className="path1" />
            <span className="path2" />
          </i>
        </div>
        <div className={s.successTitle}>You have been migrated</div>
        <div className={s.successText}>
          Please use the “Paytm Login” option available on seller login page to
          access your account.
        </div>
        <button className={s.doneBtn} onClick={() => this.closeModal()}>
          Done
        </button>
      </div>
    );
  }
}

export default OauthMigrationSuccess;
