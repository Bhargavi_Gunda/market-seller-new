import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import cx from "../../../utils/classNames";
import { connect } from "react-redux";
import get from "lodash/get";
import cloneDeep from "lodash/cloneDeep";
import s from "./OauthMigrationWrapper.scss";
import { OTP_CONFIG } from "./OauthMigrationConfig";
import RESPONSE_KEYS from "../../constants/responseKeys";
import { resendOtp, verifyOtp } from "../../actions/common";
//import { clearApiData } from '../../../store/apiActions';
//import dotLoaderImg from '../../../public/dslr/Img/dotLoader.gif';
import { SOMETHING_WENT_WRONG } from "../../constants/constants";

class OauthMigrationForm extends Component {
  static defaultProps = {
    otpUserInfo: {},
    closeModal: () => {},
    nextStepValue: 4,
    isBlockingPopUp: false,
  };
  static propTypes = {
    otpUserInfo: PropTypes.objectOf(PropTypes.any),
    closeModal: PropTypes.func,
    nextStepValue: PropTypes.number,
    changeStepValue: PropTypes.func.isRequired,
    isBlockingPopUp: PropTypes.bool,
  };

  state = {
    isOTPError: false,
    otpError: "",
    timeLimit: OTP_CONFIG.timeLimit,
    otpValue: cloneDeep(OTP_CONFIG.otpDefaultValue),
    rensendOtpSuccess: false,
  };

  componentDidMount() {
    this.startTimer();
  }

  componentWillUnmount() {
    this.stopTimer(this.intervalId);
  }
  componentDidUpdate(prevProps) {
    this.updateVerifyOTP(prevProps);
    this.updateResendOTP(prevProps);
  }
  updateVerifyOTP(prevProps) {
    const { verifyOTPResp = {} } = this.props;
    const { verifyOTPResp: verifyOTPRespOld = {} } = prevProps;
    if (
      verifyOTPRespOld.isFetching === true &&
      verifyOTPResp.isFetching === false
    ) {
      const otpError = get(verifyOTPResp, "error.error", SOMETHING_WENT_WRONG);
      const response = get(verifyOTPResp, "response", "");
      if (response) {
        this.props.changeStepValue(this.props.nextStepValue);
      } else {
        this.setState({ isOTPError: true, otpError }, () => {
          setTimeout(() => {
            this.setState({
              otpError: "",
            });
          }, 2500);
        });
      }
      this.props.clearApiData(RESPONSE_KEYS.MIGRATION_VERIFY_OTP);
    }
  }
  updateResendOTP(prevProps) {
    const { resendOTPResp = {} } = this.props;
    const { resendOTPResp: resendOTPRespOld = {} } = prevProps;
    if (
      resendOTPRespOld.isFetching === true &&
      resendOTPResp.isFetching === false
    ) {
      const otpError = get(resendOTPResp, "error.error", SOMETHING_WENT_WRONG);
      const response = get(resendOTPResp, "response", "");

      if (response) {
        this.setState(
          {
            isOTPError: false,
            otpError: "",
            rensendOtpSuccess: true,
            otpValue: cloneDeep(OTP_CONFIG.otpDefaultValue),
          },
          () => {
            setTimeout(() => {
              this.setState(
                {
                  rensendOtpSuccess: false,
                  timeLimit: OTP_CONFIG.timeLimit,
                },
                this.startTimer
              );
            }, 2500);
          }
        );
      } else {
        this.setState(
          {
            isOTPError: true,
            otpError,
            otpValue: cloneDeep(OTP_CONFIG.otpDefaultValue),
          },
          () => {
            setTimeout(() => {
              this.setState({
                otpError: "",
              });
            }, 2500);
          }
        );
      }

      this.props.clearApiData(RESPONSE_KEYS.MIGRATION_RESEND_OTP);
    }
  }

  startTimer = () => {
    this.intervalId = setInterval(() => {
      let timeLimit = this.state.timeLimit;
      if (timeLimit === 0) {
        this.stopTimer(this.intervalId);
      } else {
        timeLimit--;
        this.setState({ timeLimit });
      }
    }, 1000);
  };

  stopTimer = (intervalId) => {
    clearInterval(intervalId);
  };
  handleChange = (index, event) => {
    if (event.target.value.length > 1) {
      event.target.value = event.target.value.slice(-1);
    }
    if (event.target.value.length >= 1 && event.target.nextSibling) {
      event.target.nextSibling.select();
    }
    this.state.otpValue[index] = event.target.value;
    this.setState({ ...this.state, isOTPError: false, otpError: "" });
  };

  handleKeyPress = (event) => {
    if (event.which < 48 || event.which > 57) {
      event.preventDefault();
    }
  };
  handleKeyUp = (event) => {
    if (event.which === 8 && event.target.previousSibling) {
      event.target.previousSibling.select();
    }
  };

  convertSeconds = (totalSeconds) => {
    let mins = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    mins = mins < 10 ? `0${mins}` : mins;
    seconds = seconds < 10 ? `0${seconds}` : seconds;
    return `${mins}:${seconds}`;
  };

  resendOTP = () => {
    this.props.resendOtp(RESPONSE_KEYS.MIGRATION_RESEND_OTP);
  };
  verifyOtp = () => {
    const otpValue = this.state.otpValue.join("");
    if (otpValue.length < OTP_CONFIG.length) {
      this.setState(
        { isOTPError: true, otpError: "Please enter valid OTP" },
        () => {
          setTimeout(() => {
            this.setState({
              otpError: "",
            });
          }, 2500);
        }
      );
    } else {
      this.props.verifyOtp(
        { otp: otpValue },
        RESPONSE_KEYS.MIGRATION_VERIFY_OTP
      );
    }
  };
  getOptStatus = () => {
    if (this.state.rensendOtpSuccess) {
      return (
        <div className={s.successOtp}>
          <i className="success">
            <span className="path1" />
            <span className="path2" />
          </i>
          Otp re-sent
        </div>
      );
    }
    return this.state.timeLimit > 0 ? (
      <div className={s.resendTime}>
        Resend OTP in <span>{this.convertSeconds(this.state.timeLimit)}</span>
      </div>
    ) : (
      <a className={s.resendOtp} onClick={this.resendOTP}>
        Resend OTP
      </a>
    );
  };
  getOTPUserInfo = () => {
    const { otpUserInfo } = this.props;
    if (otpUserInfo && Number(otpUserInfo.phone) > 0) {
      return `mobile number ${otpUserInfo.phone}`;
    } else if (otpUserInfo && otpUserInfo.email && otpUserInfo.email !== "0") {
      return `email ID ${otpUserInfo.email}`;
    }
    return "";
  };

  render() {
    const { isOTPError, otpError, otpValue } = this.state;
    const { isFetching } = this.props.resendOTPResp;
    const { isFetching: isVerifyOTPFetching } = this.props.verifyOTPResp;

    const isDisabled = otpError || otpValue.join("").length < OTP_CONFIG.length;

    return (
      <div className={s.otpWrapper}>
        <div className={s.otpTitle}>Enter OTP</div>
        {!this.props.isBlockingPopUp ? (
          <div className={s.closeIcon}>
            <i
              onClick={() => this.props.closeModal()}
              role="presentation"
              className="clear"
            />
          </div>
        ) : (
          ""
        )}
        <div className={s.otpSend}>
          {`Enter the OTP sent to your registered ${this.getOTPUserInfo()}`}
        </div>
        <div className={s.otpInputInfo}>
          {Array(OTP_CONFIG.length)
            .fill(0)
            .map((input, index) => (
              <input
                key={`otpValue${index}`}
                type="number"
                maxLength="1"
                className={cx({
                  [s.error]: isOTPError,
                })}
                value={otpValue[index]}
                onKeyPress={this.handleKeyPress}
                onKeyUp={this.handleKeyUp}
                onChange={(e) => this.handleChange(index, e)}
              />
            ))}
        </div>

        {otpError ? (
          <div className={s.errorMessage}>{otpError}</div>
        ) : (
          <div className={s.resendLink}>
            {isFetching ? (
              <img className={s.fetchLoader} src="" alt="" />
            ) : (
              <Fragment>{this.getOptStatus()}</Fragment>
            )}
          </div>
        )}
        <div className={s.bottomBtn}>
          {isVerifyOTPFetching ? (
            <img className={s.fetchLoader} src="" alt="" />
          ) : (
            <button className={s.verifyBtn} onClick={this.verifyOtp}>
              Verify
            </button>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ app, apiData }) => ({
  userInfo: get(app, "userInfo", {}),
  verifyOTPResp: get(apiData, `[${RESPONSE_KEYS.MIGRATION_VERIFY_OTP}]`, {}),
  resendOTPResp: get(apiData, `[${RESPONSE_KEYS.MIGRATION_RESEND_OTP}]`, {}),
});
const mapDispatchToProps = {
  verifyOtp,
  resendOtp,
  clearApiData,
};

export default connect(mapStateToProps, mapDispatchToProps)(OauthMigrationForm);
