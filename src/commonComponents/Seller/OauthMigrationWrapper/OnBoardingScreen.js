import React, { Component } from "react";
import PropTypes from "prop-types";

import get from "lodash/get";
import s from "./OauthMigrationWrapper.scss";

class OnBoardingScreen extends Component {
  static defaultProps = {
    isEmailAlreadyRegistered: false,
    userInfo: {},
    changeStepValue: () => {},
    closeModal: () => {},
    deliveryType: "",
    nextStepValue: 2,
    isBlockingPopUp: false,
  };
  static propTypes = {
    isEmailAlreadyRegistered: PropTypes.bool,
    userInfo: PropTypes.objectOf(PropTypes.any),
    changeStepValue: PropTypes.func,
    closeModal: PropTypes.func,
    deliveryType: PropTypes.string,
    nextStepValue: PropTypes.number,
    isBlockingPopUp: PropTypes.bool,
  };
  oauthMigrationSteps = {
    emailAlreadyRegistered: [
      {
        label: "Sign in again using your Paytm password",
        subLabel:
          "Your email ID {emailID} is already registered with common Paytm login.",
      },
    ],
    emailNotRegistered: [
      {
        label: "Please check your email account",
        subLabel:
          "You would have recieved a Set Password Email from us on {emailID}",
      },
      {
        label: "Create a new password",
        subLabel: "Tap on the link in that email to create a new password",
      },
      {
        label: "Sign in again using your new password",
        subLabel:
          "Click on the button below and enter your new password to migrate your account.",
      },
    ],
  };

  render() {
    const {
      deliveryType,
      isEmailAlreadyRegistered,
      userInfo,
      nextStepValue,
      isBlockingPopUp,
    } = this.props;
    const steps = isEmailAlreadyRegistered
      ? this.oauthMigrationSteps.emailAlreadyRegistered
      : this.oauthMigrationSteps.emailNotRegistered;
    const email = get(userInfo, "email", "");
    return (
      <div className={s.migrationWrapper}>
        {!isBlockingPopUp ? (
          <div className={s.closeIcon}>
            <i
              onClick={() => this.props.closeModal()}
              role="presentation"
              className="clear"
            />
          </div>
        ) : (
          ""
        )}
        <div className={s.titleInfo}>
          {deliveryType
            ? "We will take just a minute of yours!"
            : "Paytm for Business Dashboard is our new home now!!"}
        </div>
        <div className={s.followStep}>
          {!deliveryType
            ? `Paytm stores merchant operations panel has now been moved to our new and clean Paytm for Business Dashboard. The link to our new panel is dashboard.paytm.com and to login to the panel you need to create a new login credential through which you will login to the new panel.`
            : `Migration to common Paytm login is mandatory for enabling ${deliveryType}.`}{" "}
          <span>Follow the steps below to proceed:</span>
        </div>
        <ul className={s.stepItemBox}>
          {steps.map((step, index) => (
            <li>
              <div className={s.stepCount}> {`Step ${index + 1}`}</div>
              <div className={s.headingText}>{step.label}</div>
              <div className={s.stepText}>
                {step.subLabel.replace("{emailID}", email)}
              </div>
            </li>
          ))}
        </ul>
        <button
          className={s.btnInfo}
          onClick={() => this.props.changeStepValue(nextStepValue)}
        >
          Sign In With Paytm Account
        </button>
        <div className={s.loginInfo}>
          Facing issue in login?
          <a href="mailto:paytm.orders@paytm.com">Contact us </a>
        </div>
      </div>
    );
  }
}

export default OnBoardingScreen;
