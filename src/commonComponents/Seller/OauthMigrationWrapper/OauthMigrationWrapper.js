/* eslint-disable react/jsx-no-bind */
import React, { Component } from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import get from "lodash/get";
import OnBoardingScreen from "./OnBoardingScreen";
import OauthLoginIframe from "./OauthLoginIframe";
import OauthMigrationForm from "./OauthMigrationForm";
import OauthMigrationSuccess from "./OauthMigrationSuccess";
import { STEP_VALUES } from "./OauthMigrationConfig";
import { registerUser } from "../../actions/common";
import s from "./OauthMigrationWrapper.scss";
import CenterModal from "../Common/CenterModal/CenterModal";
import RESPONSE_KEYS from "../../constants/responseKeys";
import { clearApiData } from "../../actions/apiActions";
import fetchUserInfo from "../../actions/common/fetchUserInfo";
import { logLocalError } from "./../../services/common/monitoringUtils";
import { monitoringEvents } from "./../../constants";
import showBottomModal from "../../actions/bottomModalaction";
import { isMweb } from "./../../services/common/commonUtils";

const {
  REGISTER_OAUTH_USER_RESPONSE,
  OAUTH_MIGRATION_MODAL_RENDERED,
} = monitoringEvents;
class OauthMigrationWrapper extends Component {
  static defaultProps = {
    closeModal: () => {},
    deliveryType: "",
    isMobileLayout: false,
  };
  static propTypes = {
    closeModal: PropTypes.func,
    deliveryType: PropTypes.string,
    isMobileLayout: PropTypes.bool,
  };
  state = {
    stepVal: 1,
    isEmailAlreadyRegistered: false,
    otpUserInfo: {},
    isPersonaUser: false,
  };
  menuStyle = {
    maxHeight: 95,
    bottom: 0,
    contentPadding: 0,
    clearPrevious: false,
    popupZindex: 8,
    overflow: "scroll",
  };
  componentDidUpdate(prevProps) {
    this.setregisterUserData(prevProps);
    this.fetchIsPersona(prevProps);
  }
  componentDidMount = () => {
    window.top.addEventListener("message", this.loadOtpcreen);
    this.props.fetchUserInfo();
    this.props.registerUser(RESPONSE_KEYS.REGISTER_OAUTH_USER);
  };
  componentWillUnmount() {
    window.removeEventListener("message", this.loadOtpcreen);
  }

  loadOtpcreen = (event) => {
    if (
      event.data.msg === "OauthIframe" &&
      event.origin === `${location.protocol}//${location.host}`
    ) {
      const userInfo = event.data.renderData;
      if (userInfo.stepValue === 4) {
        this.setState({
          stepVal: STEP_VALUES.SUCCESS,
        });
      } else {
        this.setState({
          stepVal: STEP_VALUES.MIGRATION_OTP,
          otpUserInfo: {
            email: (userInfo && userInfo.email) || null,
            phone: (userInfo && userInfo.phone) || null,
          },
        });
      }
    }
  };
  setregisterUserData = (prevProps) => {
    const { registerUserResp = {} } = this.props;
    const { registerUserResp: registerUserRespOld = {} } = prevProps;
    if (registerUserRespOld.isFetching && !registerUserResp.isFetching) {
      const status = get(registerUserResp, "response.status", {});
      const { userInfo } = this.props;
      const customMessage = JSON.stringify({
        mid: get(userInfo, "merchant_id", ""),
        email: get(userInfo, "email", ""),
        hasMigrationPermission: get(userInfo, "permissions", []).includes(
          "PERSONA-TO-OAUTH-MIGRATION"
        ),
        registerUserResponse: registerUserResp,
      });
      logLocalError(REGISTER_OAUTH_USER_RESPONSE, customMessage);
      if (status === 417) {
        this.setState({ isEmailAlreadyRegistered: true });
      }
    }
  };
  fetchIsPersona = (prevProps) => {
    const { userInfoResp = {} } = this.props;
    const { userInfoResp: userInfoRespOld = {} } = prevProps;
    if (userInfoRespOld.isFetching && !userInfoResp.isFetching) {
      const { status, response = {} } = userInfoResp;
      if (status === 200 && response.success) {
        const isPersonaUser = get(
          response,
          "success.user.isPersonaUser",
          false
        );

        this.setState({ isPersonaUser }, () => {
          const { stepVal } = this.state;
          const { isMobileLayout, deliveryType, userInfo } = this.props;
          const hasMigrationPermission = get(
            userInfo,
            "permissions",
            []
          ).includes("PERSONA-TO-OAUTH-MIGRATION");
          const showMigrationPopup =
            hasMigrationPermission && isMweb() && isMobileLayout;
          if (isPersonaUser && deliveryType) {
            const customMessage = JSON.stringify({
              hasMigrationPermission,
              mid: get(userInfo, "merchant_id", ""),
              email: get(userInfo, "email", ""),
            });
            logLocalError(OAUTH_MIGRATION_MODAL_RENDERED, customMessage);
          }
          if (showMigrationPopup) {
            this.props.showBottomModal(
              true,
              isPersonaUser
                ? this.renderOauthScreens(stepVal, true)
                : this.renderOauthScreens(0, true),
              this.menuStyle
            );
          }
        });
      }
    }
  };
  changeStepValue = (stepVal) => {
    this.setState({ stepVal });
  };
  closeModal = (ismobile) => {
    if (ismobile) {
      this.props.showBottomModal(false);
    }
    this.props.closeModal();
  };

  renderOauthScreens = (stepVal, ismobile = false) => {
    const { deliveryType, userInfo, permissions } = this.props;
    const { otpUserInfo } = this.state;
    const isBlockingPopUp = permissions.includes(
      "PERSONA-TO-OAUTH-MIGRATION-BLOCKER"
    );

    switch (stepVal) {
      case 0:
      case 1:
        return (
          <OnBoardingScreen
            deliveryType={deliveryType}
            changeStepValue={this.changeStepValue}
            userInfo={userInfo}
            nextStepValue={STEP_VALUES.LOGIN_IFRAME}
            isEmailAlreadyRegistered={this.state.isEmailAlreadyRegistered}
            closeModal={() => this.closeModal(ismobile)}
            isBlockingPopUp={isBlockingPopUp}
          />
        );
      case 2:
        return (
          <OauthLoginIframe
            closeModal={this.closeModal}
            isBlockingPopUp={isBlockingPopUp}
          />
        );
      case 3:
        return (
          <OauthMigrationForm
            otpUserInfo={otpUserInfo}
            changeStepValue={this.changeStepValue}
            nextStepValue={STEP_VALUES.SUCCESS}
            closeModal={() => this.closeModal(ismobile)}
            isBlockingPopUp={isBlockingPopUp}
          />
        );
      case 4:
        return (
          <OauthMigrationSuccess
            closeModal={() => this.closeModal(ismobile)}
            isBlockingPopUp={isBlockingPopUp}
          />
        );

      default:
        return (
          <OnBoardingScreen
            deliveryType={deliveryType}
            changeStepValue={this.changeStepValue}
            userInfo={userInfo}
            nextStepValue={STEP_VALUES.LOGIN_IFRAME}
            isEmailAlreadyRegistered={this.isEmailAlreadyRegistered}
            closeModal={() => this.closeModal(ismobile)}
            isBlockingPopUp={isBlockingPopUp}
          />
        );
    }
  };

  render() {
    const { stepVal, isPersonaUser } = this.state;
    const { isMobileLayout, userInfoResp = {} } = this.props;
    const showMigrationPopup =
      !userInfoResp.isFetching &&
      this.props.permissions.includes("PERSONA-TO-OAUTH-MIGRATION") &&
      ((!isMweb() && isMobileLayout) || !isMobileLayout);
    return showMigrationPopup ? (
      <CenterModal
        overlay
        showCross={false}
        showViewButton={false}
        showIcon={false}
        maxHeight={90}
        overflow="auto"
        width="70%"
        classes={s.modalContainer}
        borderRadius={15}
        position="fixed"
        zIndex={1005}
        closeFn={this.props.closeModal}
      >
        {isPersonaUser
          ? this.renderOauthScreens(stepVal)
          : this.renderOauthScreens(0)}
      </CenterModal>
    ) : (
      ""
    );
  }
}
const mapStateToProps = ({ app, apiData }) => ({
  userInfo: get(app, "userInfo", {}),
  registerUserResp: get(apiData, `[${RESPONSE_KEYS.REGISTER_OAUTH_USER}]`, {}),
  roles: get(app, `userInfo.roles`, []),
  updateregisterUserResp: get(
    apiData,
    `[${RESPONSE_KEYS.UPDATE_MERCHANT_HAND_HOLDING}]`,
    {}
  ),
  userInfoResp: get(apiData, `[${RESPONSE_KEYS.USER_INFO}]`, {}),
  permissions: get(app, "userInfo.permissions"),
});

const mapDispatchToProps = {
  registerUser,
  clearApiData,
  fetchUserInfo,
  showBottomModal,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OauthMigrationWrapper);
