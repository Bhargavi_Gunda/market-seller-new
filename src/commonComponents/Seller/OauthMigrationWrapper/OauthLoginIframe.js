import React, { Component } from "react";

import PropTypes from "prop-types";
import s from "./OauthMigrationWrapper.scss";
import { urlEndPoints } from "./../../constants";

const { domain } = urlEndPoints;

class OauthLoginIframe extends Component {
  static defaultProps = {
    closeModal: () => {},
    isBlockingPopUp: false,
  };
  static propTypes = {
    closeModal: PropTypes.func,
    isBlockingPopUp: PropTypes.bool,
  };
  render() {
    return (
      <div className={s.migrationWrapperIframe}>
        {!this.props.isBlockingPopUp ? (
          <div className={s.closeIcon}>
            <i
              onClick={() => this.props.closeModal()}
              role="presentation"
              className="clear"
            />
          </div>
        ) : (
          ""
        )}
        <iframe
          title="OauthMigration"
          src={domain.oauthDNSwithDetails}
          height="750px"
          width="600px"
        />
      </div>
    );
  }
}

export default OauthLoginIframe;
