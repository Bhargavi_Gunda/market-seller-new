import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import s from "./Header.module.scss";
import business_with_Paytm from './assets/business_with_Paytm_new.svg'
import NewUserDetails from '../NewUserDetails/NewUserDetails';
 import CommonDropdownContext from '../../../components/common/CommonDropdownContext/CommonDropdownContext';
import HamburgerMenu from "../HamburgerMenu/HamburgerMenu";
//import agent from '../../constants/agent';

class Header extends PureComponent {
  static propTypes = {
    userData: PropTypes.objectOf(PropTypes.any).isRequired,
    leftPanelData: PropTypes.arrayOf(PropTypes.any).isRequired,
  };

  constructor(props){
    super(props);
    this.headerRef = React.createRef();
  }

  state = {
    showModal: false,
  };

  getInitials() {
    const {
      userData: { email, username },
    } = this.props;
    let initials = '';
    if (username) {
      const splits = username.trim().split(' ');
      initials = splits[0][0];
      if (splits.length > 1) {
        initials += splits[splits.length - 1][0] || '';
      }
    }
    return initials
      ? initials.toUpperCase()
      : email && email.split('')[0].toUpperCase();
  }

  setModalState = bool => {
    this.setState({
      showModal: bool,
    });
  };

  render() {
    const {
      userData: { email, username },
      leftPanelData = {},
    } = this.props;
    const { showModal } = this.state;

    const userNameInitial = this.getInitials();
    const userLabel = <div className={s.userLabel}>{userNameInitial}</div>;
    return (
        <div className={s.headerNew}>
        {<HamburgerMenu leftPanelData={leftPanelData} />}
        <a href="#">
          <img src={business_with_Paytm} className='business_with_Paytm'/>
            <span className="path1" />
            <span className="path2" />
        </a>
        <div className={s.login}>
          {!showModal && (
            <span onClick={() => this.setModalState(true)} className={s.icon}>
              {userNameInitial}
            </span>
          )}
          {showModal && (
            <CommonDropdownContext
              onOutsideClick={() => this.setModalState(false)}
              label={userLabel}
            >
              <NewUserDetails
                onSelection={() => {
                  this.setModalState(false);
                }}
                userEmail={email}
                userName={username}
              />
            </CommonDropdownContext>
          )}
        </div>
        </div>
    );
  }
}

export default Header;
