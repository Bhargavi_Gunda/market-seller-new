import React from 'react';
import PropTypes from 'prop-types';
// import includes from 'lodash/includes';
// import history from '../../history';

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

class Link extends React.Component {
  static propTypes = {
    to: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string,
    openInNewtab: PropTypes.bool,
  };

  static defaultProps = {
    onClick: null,
    className: '',
    openInNewtab: false,
  };

  handleClick = event => {
    if (window.Hawkeye) {
      window.Hawkeye.log('pageNavigationStart');
      const { to } = this.props;
      if(to) {
        window.location.href = to; // hard reload scenario
      }
    
    if (this.props.onClick) {
      this.props.onClick(event);
    }

    if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();
    to && history.push(to);
  }
};

  // handleClick = event => {
  //   const { to, onClick, target, navigate, method, scrollToTop } = _.pick(
  //     this.props,
  //     ['to', 'onClick', 'target', 'navigate', 'method', 'scrollToTop'],
  //   );
  //   if ((isAbsoluteURL(to) && target !== '_blank') || target === '_self') {
  //     window.location.href = to; //hard reload scenario
  //     return;
  //   }
  //   let allowTransition = true;

  //   onClick && onClick();

  //   if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
  //     return;
  //   }

  //   if (event.defaultPrevented === true) {
  //     allowTransition = false;
  //   }

  //   if (target !== '_blank') {
  //     event.preventDefault();
  //     if (allowTransition) {
  //       if (to) {
  //         navigate(to, method, scrollToTop);
  //       }
  //     }
  //   }
  // };

  render() {
    const { children, className, ...props } = this.props;
    return (
      <a className={className} {...props} onClick={this.handleClick}>
        {children}
      </a>
    );
  }
}

export default Link;
