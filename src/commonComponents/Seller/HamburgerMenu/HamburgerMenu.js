import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import cx from "../../../utils/classNames";
import s from './HamburgerMenu.module.scss';
import LeftPanel from "../LeftPanel/LeftPanel";
import LeftPanelWrapper from "../LeftPanelWrapper/LeftPanelWrapper";
// import OutsideClickAlerter from '../Common/OutsideClickAlerter';
class HamburgerMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
    };
  }

  onMenuOpen = () => {
    this.setState({ menuOpen: true });
  };

  onMenuClose = () => {
    this.setState({ menuOpen: false });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.activeTab !== this.props.activeTab && this.props.activeTab) {
      this.onMenuClose();
    }
  }
  

  render() {
    // const { leftPanelData } = this.props;
    const { menuOpen } = this.state;
    return (
      <section className={s.hamburgerMenu}>
        <i className={cx(s.menu, "menu")} onClick={this.onMenuOpen} />
        <LeftPanelWrapper showModal={menuOpen} onClickClose={this.onMenuClose}>
          <LeftPanel activeTab={'/growth'}/>
        </LeftPanelWrapper>
      </section>
     
    );
  }
}

const mapStateToProps = () => ({
  activeTab: '/Growth',
});

HamburgerMenu.propTypes = {
  leftPanelData: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default connect(mapStateToProps, null)(HamburgerMenu);
