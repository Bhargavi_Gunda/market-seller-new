import React, { Component } from 'react';
// import withStyles from 'isomorphic-style-loader/lib/withStyles';
// import get from 'lodash/get';
import { connect } from 'react-redux';
import s from './NewUserDetails.scss';
// import responseKeys from '../../constants/responseKeys';
// import { setSelectedWarehouse } from '../../actions/merchantController';
// import postman from '../../postman';
// import { CHANGE_WAREHOUSE } from '../../constants/constants';
// import { getInitials } from '../../services/common/commonUtils';
// import agent from '../../constants/agent';
import { logOutUser } from '../../../services/common/coreUtils';
// import emitPollingjsEvent from '../../h5Utils/emitPollingjsEvent';
// import {
//   Accordion,  href="/forcelogout"
//   AccordionBox,
// } from '../../components/Common/NewAccordion/NewAccordion';
// import WarehouseList from './WarehouseList';

const lowerCaseTrim = (string = '') => string.toLowerCase().trim();

// TODO: Rename this component in future
class NewUserDetails extends Component {
  state = {
    searchValue: '',
  };

  renderSearch = () => (
    <div className={s.boxSearchDrop}>
      <div className={s.searchDrop}>
        <input
          type="test"
          placeholder="Search by Name"
          value={this.state.searchValue}
          onChange={e => {
            const {
              target: { value },
            } = e;
            this.setState({
              searchValue: value,
            });
          }}
          autoFocus
        />
        <i className="search" />
      </div>
    </div>
  );

  renderWarehouseList = (
    // warehouseList = [],
    // merchantData = null,
    // showInitials = false,
    // isSingleMerchant,
  ) => {
    // const { onSelection } = this.props;
    return (
      // <WarehouseList
      //   showInitials={showInitials}
      //   isSingleMerchant={isSingleMerchant}
      //   merchantData={merchantData}
      //   data={warehouseList}
      //   onWarehouseSelect={() => onSelection()}
      // />
      <>
      </>
    );
  };

  renderSearchWarehouse = (merchantWarehouseDetails, searchValue) => {
    let modifiedMerchantList = [
      {
        key: 'searchList',
        warehouseList: [],
      },
    ];
    merchantWarehouseDetails &&
      merchantWarehouseDetails.map(data => {
        const { warehouse: warehouseList = [] } = data;
        warehouseList
          .filter(
            iData =>
              lowerCaseTrim(iData.name).indexOf(lowerCaseTrim(searchValue)) >
              -1,
          )
          .map(warehouse => {
            warehouse['merchantData'] = data.merchant;
            modifiedMerchantList[0].warehouseList.push(warehouse);
          });
      });
    if (modifiedMerchantList[0].warehouseList.length > 0) {
      return modifiedMerchantList.map(() => {
        return (
          // <div className={s.listBox}>
          //   {this.renderWarehouseList(item.warehouseList, null, true, true)}
          // </div>
          <>
          </>
        );
      });
    } else {
      return [];
    }
  };

  renderWareHouse = () => {
    const {
      // wareHouseDetails = [],
      // onSelection,
      merchantWarehouseDetails = [],
      isMultiMerchant,
    } = this.props;
    const { searchValue } = this.state;
    /* 
      Code to handle Search Logic starts
    */
    if (lowerCaseTrim(searchValue).length > 0) {
      return this.renderSearchWarehouse(merchantWarehouseDetails, searchValue);
      /* 
      Code to handle Search Logic Ends
    */
    } else {
      return (
        merchantWarehouseDetails &&
        merchantWarehouseDetails.map(data => {
          const { id: mid = '' } = data.merchant;
          const { warehouse: warehouseList = [] } = data;
          return isMultiMerchant ? (
            <div className={s.listBox} role="button" tabIndex={0} key={mid}>
              {/* <AccordionBox key={mid}>
                <Accordion
                  header={
                    <div className={s.itemRow}>
                      <div className={s.userPic}>
                        {getInitials(merchant_name)}
                      </div>
                      <div className={s.merchantName}>{merchant_name}</div>
                    </div>
                  }
                  mountOnOpen
                > */}
                  {/* {this.renderWarehouseList(warehouseList, data.merchant)} */}
                {/* </Accordion> */}
              {/* </AccordionBox> */}
            </div>
          ) : (
            <div className={s.listBox}>
              {this.renderWarehouseList(
                warehouseList,
                data.merchant,
                true,
                true,
              )}
            </div>
          );
        })
      );
    }
  };

  // renderWareHouses = () => {
  //   const { wareHouseDetails = [], onSelection } = this.props;
  //   return (
  //     wareHouseDetails &&
  //     wareHouseDetails
  //       .map(warehouse => {
  //         const { warehouse_id: wid = '', name = '', address = '' } = warehouse;
  //         const { searchValue } = this.state;
  //         const dontShow =
  //           searchValue &&
  //           lowerCaseTrim(name).indexOf(lowerCaseTrim(searchValue)) < 0;
  //         if (dontShow) return null;
  //         return (
  //           <div
  //             className={s.listBox}
  //             role="button"
  //             tabIndex={0}
  //             key={wid}
  //             // onClick={() => {
  //             //   this.props.setSelectedWarehouse(warehouse);
  //             //   const mid = get(this, 'props.merchantId', '');
  //             //   !agent.isExpH5 &&
  //             //     localStorage.setItem(
  //             //       'selectedWarehouse',
  //             //       JSON.stringify({
  //             //         selectedWarehouse: warehouse,
  //             //         mid,
  //             //       }),
  //             //     );
  //             //   !agent.isExpH5 &&
  //             //     emitPollingjsEvent('send', 'restartPollingSetWid', {
  //             //       selectedWarehouse: {
  //             //         selectedWarehouse: warehouse,
  //             //         mid,
  //             //       },
  //             //     });
  //             //   postman.publish(CHANGE_WAREHOUSE, warehouse);
  //             //   onSelection();
  //             // }}
  //           >
  //             <AccordionBox key={wid}>
  //               <Accordion
  //                 header={
  //                   <div className={s.itemRow}>
  //                     <div className={s.userPic}>{getInitials(name)}</div>
  //                     <div className={s.merchantName}>{name}</div>
  //                   </div>
  //                 }
  //                 mountOnOpen
  //               >
  //                 <WarehouseList />
  //                 {/* <div className={s.listBoxData}>
  //                   <div className={s.listDetail}>
  //                     <div className={s.nameText}>
  //                       {name} <i className="done"></i>
  //                     </div>
  //                     <div className={s.addressInfo}>{address}</div>
  //                   </div>
  //                 </div> */}
  //               </Accordion>
  //             </AccordionBox>
  //           </div>
  //         );
  //       })
  //       .filter(Boolean)
  //   );
  // };

  signOut = () => {
     logOutUser();
  };

  render() {
    const {
      userEmail,
      userName = '',
      // wareHouseDetails = [],
      showLogout = true,
      merchantWarehouseDetails = [],
    } = this.props;
    const { searchValue } = this.state;
    const list = this.renderWareHouse() || [];
    const showSearch = !!(
      merchantWarehouseDetails && merchantWarehouseDetails.length
    );
    return (
      <section className={s.dropdownmore}>
        <div className={s.nameCon}>
          {userName && <span className={s.nameInfo}>{userName}</span>}
          <span className={s.emailInfo}>{userEmail}</span>
        </div>
        {showSearch && this.renderSearch()}
        <div className={s.merchantList}>
          <div className={s.listScroll}>
            {list.length || !searchValue ? (
              list
            ) : (
              <div className={s.noResults}>
                <i className="search" /> No Results found for &#39;
                {searchValue.trim()}&#39;
                <span>Tip: check if the search query is spelled correctly</span>
              </div>
            )}
          </div>
          {showLogout && (
            <a href="#" onClick={this.signOut}>
              Sign Out
            </a>
          )}
        </div>
        {/* <a href="/new/profile_new">Profile</a> */}
      </section>
    );
  }
}

const mapStateToProps = state => ({
    state
  // merchantId: get(state, 'app.userInfo.merchant_id'),
  
});

export default
  connect(mapStateToProps, {
  })(NewUserDetails)
