import React, { Component } from "react";

import cx from "../../../utils/classNames";
import s from "./LeftPanelWrapper.module.scss";
import { disableBodyScrolling } from "../../../utils/domUtils";
import business_with_Paytm from './assets/business_with_Paytm_new.svg'

class LeftPanelWrapper extends Component {
  componentDidMount() {
    disableBodyScrolling(true);
  }
  UNSAFE_componentWillReceiveProps = (nextProps) => {
    disableBodyScrolling(nextProps.showModal);
  };
  componentWillUnmount() {
    disableBodyScrolling(false);
  }

  handleModalClose = (evt) => {
    if (evt.target.id === "modal") {
      this.props.onClickClose();
    }
  };

  render() {
    const modalAnimationStyle = {
      width: this.props.showModal ? "175%" : 0,
      left: this.props.showModal ? "0" : "-75%",
    };
    return (
      <div
        className={s.ModalContainer}
        id="modal"
        role="presentation"
        style={modalAnimationStyle}
        onClick={(e) => this.handleModalClose(e)}
      >
        <div className={s.Modal}>
          <div className={s.ModalHeader}>
            {/* <span
              className={s.ModalCloseButton}
              onClick={this.props.onClickClose}
              role="presentation"
            >
              <i className="clear" />
            </span> */}
            <div className={s.tabHeader}>
              <div
                className={s.arrowBox}
                onClick={this.props.onClickClose}
                role="presentation"
              >
                <span className={cx(s.backArrow, s.arrowBar, s.left)} />
              </div>
              <img src={business_with_Paytm} className='business_with_Paytm'/>
                <span className="path1" />
                <span className="path2" />
              
            </div>
          </div>

          <div className={s.ModalBody}>{this.props.children}</div>
        </div>
      </div>
    );
  }
}

export default LeftPanelWrapper;
