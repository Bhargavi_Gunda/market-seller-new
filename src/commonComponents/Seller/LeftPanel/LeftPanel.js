import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import cx from "../../../utils/classNames";
import forEach from "lodash/forEach";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import replace from "lodash/replace";
import includes from "lodash/includes";
import s from "./LeftPanel.module.scss";
import Link from '../Link';
import * as actions from '../../../actions/LeftPanelActions';
import {checkPermissionLeftPanelInfo} from '../../../services/common/userServices';
import {fetchUserInfo, fetchLeftPanelInfo} from '../../../services/fetchUserInfo';
import axios from 'axios';
import { sessionObjActionCreator } from "../../../actions/LeftPanelActions";
import { FORCE_LOGOUT_ROUTE } from "../../../constants/constants";

class LeftPanel extends PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.any).isRequired,
    activeTab: PropTypes.objectOf(PropTypes.any).isRequired,
    setSelectedTab: PropTypes.func.isRequired,
    setLeftPanelSelectedTab: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.toggleSubvertical = false;
    this.state = {
      leftPanelData: '',
      activeTabs: '/growth',
    };
  }

  componentDidMount() {
    let tabFilterRawData = fetchUserInfo();
    tabFilterRawData.then((tabData) =>{
      let tabs = checkPermissionLeftPanelInfo(tabData[0] && tabData[0].data && tabData[0].data, tabData[1].data && tabData[1].data.data);
      this.setState({
        leftPanelData : tabs
      })
      this.props.setUserData(tabData[0].data.success.user);
    }).catch((err) =>{
      if(err.response.status == 403 ) {
        alert("Please login and try again")
        window.location.href = FORCE_LOGOUT_ROUTE
      }
    })
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    if (!isEqual(nextProps.activeTab, prevState.activeTabs)) {
      const updatedVerticalArr = cloneDeep(prevState.leftPanelData);
      forEach(updatedVerticalArr, (el) => {
        if (el.name === nextProps.activeTab.tab) {
          el.verticalStatus = true;
        }
      });
      return {
        activeTabs: '/growth',
        leftPanelData: updatedVerticalArr,
      };
    }
    return null;
  };

  handleToggleSubVertical = (data) => {
    const updatedVerticalArr = cloneDeep(this.state.leftPanelData);

    forEach(updatedVerticalArr, (el) => {
      if (el.name === data.name) {
        el.verticalStatus = !data.verticalStatus;
      }
    });
    this.setState({
      leftPanelData: updatedVerticalArr,
    });

    this.props.setActiveTabDetails(data)

    if (!data.subVerticals)
    this.props.setSelectedTab({ tab: data.name, subTab: null });
  };

  render() {
    const {
      leftPanelData,
     activeTabs: { tab: activeTab, subTab: activeSubTab },
    } = this.state ;

    return (
      leftPanelData && (
        <section className={s.menu}>
          <nav>
            {leftPanelData.map((tab) => {
              const subVerticals = tab.subVerticals ? tab.subVerticals : [];
              const path = subVerticals.length <= 0 ? tab.link : "";
              if(tab.name == "account" || tab.name == "travelCST" || tab.name ==  "c2i") {
                tab.imageSvg = 'icon-Account';
              }
              if(tab.name == "installations") {
                tab.imageSvg = 'icon-installation';
              }
              if(tab.name == "cod") {
                tab.imageSvg = 'icon-COD';
              }
              if(tab.name == "seo") {
                tab.imageSvg = 'icon-SEO';
              }
              if(tab.name == "courier") {
                tab.imageSvg = 'icon-Courier';
              }
              if(tab.name == "paymentLinks" || tab.name == "giftCards") {
                tab.imageSvg = 'icon-Payments';
              }
              let icons = replace(tab.imageSvg, "icon-", "");
              return (
                <React.Fragment key={tab.name}>
                  <Link
                    to={path}
                    className={activeTab === tab.name ? s.active : s.inactive}
                    onClick={() => this.handleToggleSubVertical(tab)}
                    openInNewtab={!includes(path, "/")}
                  >
                    <img className={cx(icons, s.icons)} src={require(`./leftPannel/${icons}.svg`).default} ></img>

                    <span className={s.label}>{tab.label}</span>
                    <span className={s.planArrow}>
                      {subVerticals.length > 0 && (
                        <i
                          className={cx(tab.verticalStatus ? [s.up] : [s.down])}
                        />
                      )}
                    </span>
                  </Link>
                  <div className={tab.verticalStatus ? s.openTab : s.hideTab}>
                    <div className={isEmpty(subVerticals) ? "" : s.tabOpt}>
                      { subVerticals.map((subTab) => (
                        <Link
                          to={subTab.link}
                          key={subTab.name}
                          className={
                            activeSubTab === subTab.name ? s.active : s.inactive
                          }
                          openInNewtab={!includes(subTab.link, "/")}
                          onClick={() =>
                            this.props.setSelectedTab({
                              tab: tab.name,
                              subTab: subTab.name,
                            })
                          }
                        >
                          <i className={subTab.imageSvg} />
                          <span className={s.label}>{subTab.label}</span>
                         </Link>
                      ))} 
                    </div>
                  </div>
                </React.Fragment>
              );
            })}
          </nav>
        </section>
      )
    );
  }
}

// Not needed yet
const mapStateToProps = (state) => {
  return {
    activeTabName : state
  }
};

const mapDispatchToProps =  (dispatch) =>{
  return{
        setActiveTabDetails : (tabDetails) => {
        dispatch(actions.setActiveTabDetails(tabDetails));
      },
      setUserData : (userInfo) => {
        dispatch(actions.sessionObjActionCreator(userInfo));
      },

      setSelectedTab : (tabInfo) => {
        dispatch(actions.setSelectedTab());
      }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftPanel);
