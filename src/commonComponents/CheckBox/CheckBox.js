import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "../../utils/classNames";
import styles from "./Checkbox.module.scss";

class Checkbox extends Component {
  handleChange = (evt) => {
    const {
      props: { onChange },
    } = this;
    const {
      target: { checked },
    } = evt;
    if (onChange) onChange(checked, evt);
  };

  render() {
    const {
      props: { label, isChecked, infoIcon, disabled },
    } = this;
    return (
      <div className={cx(styles.checkbox, disabled ? styles.disabled : "")}>
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <label>
          <input
            type="checkbox"
            onChange={this.handleChange}
            checked={isChecked}
            disabled={disabled}
          />
          <i className={styles.helper} />

          {label}
          {infoIcon}
        </label>
      </div>
    );
  }
}

Checkbox.propTypes = {
  /** Label name for the checkbox */
  label: PropTypes.string,
  /** handler function for onChange */
  onChange: PropTypes.func,
  /** checked value true | false */
  isChecked: PropTypes.bool,
  infoIcon: PropTypes.node,
};

Checkbox.defaultProps = {
  label: "",
  onChange: null,
  isChecked: false,
  infoIcon: "",
};

export default Checkbox;
