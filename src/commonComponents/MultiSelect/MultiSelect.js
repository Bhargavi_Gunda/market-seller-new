/* eslint-disable react/forbid-prop-types */
import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "../../utils/classNames";

import s from "./MultiSelect.styles.scss";
import CheckBox from "../CheckBox/CheckBox";

class MultiSelect extends Component {
  state = {};

  constructor(props) {
    super(props);
    this.dropdown = React.createRef();
  }

  componentDidMount = () => {
    document.body.addEventListener("click", this.clickOutSide);
    this.onSearchKeyChange();
  };

  componentWillUnmount = () => {
    document.body.removeEventListener("click", this.clickOutSide);
  };

  handleSelected = (item, isRemove) => {
    const { value = [], onChange } = this.props;
    const newVal = value;
    const itemIndex = value.findIndex((el) => el.id === item.id);
    if (itemIndex > -1) {
      newVal.splice(itemIndex, 1);
    } else if (!isRemove) {
      newVal.push(item);
    }
    onChange(newVal);
  };

  onSearchKeyChange = (key = "") => {
    const { itemList = [] } = this.props;
    let filteredItems = itemList;
    const enhancedKey = key.toLowerCase().trim();
    if (enhancedKey) {
      filteredItems = itemList.filter(
        ({ text }) => text && text.toLowerCase().includes(enhancedKey)
      );
    }
    this.setState({ filteredItems });
  };

  handleDropdown = (e) => {
    if (!e.defaultPrevented) {
      this.setState({
        showOptionList: !this.state.showOptionList,
      });
    }
  };

  getDisplayVal = () => {
    const { value = [] } = this.props;
    const displayVal = "";
    if (value.length === 1) {
      return value[0].text;
    } else if (value.length > 1) {
      return `${value[0].text} [+${value.length - 1}]`;
    }
    return displayVal;
  };

  // it will fires on outside click  check didmount
  clickOutSide = (evt) => {
    if (this.dropdown && !this.dropdown.contains(evt.target)) {
      this.setState({
        showOptionList: false,
      });
    }
  };

  render() {
    const { showOptionList, filteredItems = [] } = this.state;
    const {
      error,
      id,
      label,
      addLabel,
      dropdownHeight,
      disabled,
      required,
      value = [],
    } = this.props;
    return (
      <>
        <div
          className={cx({
            [s.disabled]: disabled,
            [s.redError]: error,
          })}
          ref={(input) => {
            this.dropdown = input; // based on ref checking if click is inside or outside
          }}
        >
          <div
            className={cx(s.floatingLabel, {
              [s.borderBlue]: !showOptionList && !error,
            })}
            role="presentation"
            onClick={this.handleDropdown}
          >
            <div className={s.storeSection}>
              <div
                className={cx(
                  s.storeLabel,
                  s.selectedStore,
                  required ? s.required : ""
                )}
              >
                {label}
              </div>
              <div className={s.addStoreBtn}>
                {value.length ? "Add More" : addLabel}
              </div>
            </div>
            <div
              className={
                !showOptionList && value?.length ? s.selectedState : ""
              }
            >
              {value.map((el) => (
                <div
                  className={s.selectedItem}
                  style={{
                    display: !showOptionList ? "" : "none",
                  }}
                >
                  <div className={s.itemDataSelected}>
                    <div className={s.itemName}>{el.text}</div>
                    <div className={s.itemAddress}>{el.address}</div>
                  </div>
                  <div
                    className={s.removeBtn}
                    onClick={(e) => {
                      e.preventDefault();
                      this.handleSelected(el, true);
                    }}
                  >
                    Remove
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className={s.dropdown}>
            <div
              className={s.divOpen}
              id="dropdown"
              style={{
                display: showOptionList ? "" : "none",
              }}
            >
              <div>
                <div
                  className={s.listBox}
                  style={{
                    maxHeight: dropdownHeight,
                  }}
                >
                  <div className={s.searchInfo}>
                    <i className="search" />
                    <input
                      ref={this.inputRef}
                      autoComplete="not-required"
                      className={s.floatingInput}
                      type="text"
                      placeholder=" "
                      id={id}
                      onChange={(e) => this.onSearchKeyChange(e.target.value)}
                    />
                  </div>
                  <div className={s.ddOptions}>
                    {filteredItems.map((item) => (
                      <div
                        className={cx(s.dropList, {
                          [s.selected]:
                            value.findIndex((el) => el.id === item.id) > -1,
                        })}
                        key={item.id}
                        role="presentation"
                      >
                        <div className={s.checkboxInfo}>
                          <CheckBox
                            key={item.id}
                            isChecked={
                              value &&
                              value.findIndex((el) => el.id === item.id) > -1
                            }
                            onChange={() => this.handleSelected(item)}
                            // onChange={val => on(val, id)}
                            // label={item.text}
                            disabled={disabled}
                          />
                        </div>
                        <div className={s.checkBoxData}>
                          <div className={s.itemText}>{item.text}</div>
                          <div className={s.itemDetailsText}>
                            {item.address}
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {error ? <div className={s.Error}>{error}</div> : null}
      </>
    );
  }
}

MultiSelect.propTypes = {
  disabled: PropTypes.any,
  dropdownHeight: PropTypes.string,
  error: PropTypes.string,
  id: PropTypes.string,
  itemList: PropTypes.array,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool,
  value: PropTypes.array,
  addLabel: PropTypes.string,
};

MultiSelect.defaultProps = {
  disabled: false,
  dropdownHeight: "",
  error: "",
  id: "",
  itemList: [],
  label: "",
  required: false,
  value: [],
  addLabel: "",
};

export default MultiSelect;
