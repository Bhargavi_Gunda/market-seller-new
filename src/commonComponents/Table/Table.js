import React, { Component } from "react";

import PropTypes, {
  /* tree-shaking no-side-effects-when-called */ arrayOf,
  /* tree-shaking no-side-effects-when-called */ shape,
  /* tree-shaking no-side-effects-when-called */ instanceOf,
  /* tree-shaking no-side-effects-when-called */ oneOfType,
} from "prop-types";
import cx from "../../utils/classNames";
import Pagination from "./Pagination/Pagination";
// eslint-disable-next-line
import styles from "./Table.module.scss";

const TableHeader = ({ columns = [] }) => (
  <div className={styles.tableHeader}>
    {columns.map((d) => {
      const { type = "col1", id, header } = d;
      return (
        <div className={cx([styles[type]])} key={`${id}_${header}`}>
          {header}
        </div>
      );
    })}
  </div>
);

TableHeader.propTypes = {
  columns: arrayOf(instanceOf(Object)).isRequired,
};

const TableData = ({
  data = [],
  columns = [],
  isFetching,
  noDataComponent,
  onRowClick,
  onKeyDown,
}) => {
  if (isFetching) {
    return <div className={styles.loaderGif} />;
  }
  if (data.length > 0 && columns.length > 0) {
    return data.map((row, index) => (
      <div
        tabIndex={index}
        role="button"
        className={styles.tableRow}
        key={`${row.id}`}
        onKeyDown={(event) => {
          if (onKeyDown) onKeyDown(row, event);
        }}
        onClick={(event) => {
          if (onRowClick) onRowClick(row, event);
        }}
      >
        {columns.map((col) => {
          const { type = "col1" } = col;
          return (
            <div
              className={cx([styles[type]])}
              key={`${row[col.accessor]}_${col.id}`}
            >
              {col.accessor in row ? row[col.accessor] : ""}
            </div>
          );
        })}
      </div>
    ));
  }
  return <div className={styles.noData}>{noDataComponent}</div>;
};
TableData.propTypes = {
  data: arrayOf(instanceOf(Object)).isRequired,
  columns: arrayOf(instanceOf(Object)).isRequired,
  isFetching: PropTypes.bool,
  noDataComponent: oneOfType([PropTypes.node, PropTypes.string]),
  onRowClick: PropTypes.func,
  onKeyDown: PropTypes.func,
};
TableData.defaultProps = {
  isFetching: false,
  noDataComponent: "No data to show.",
  onRowClick: null,
  onKeyDown: null,
};

class Table extends Component {
  render() {
    const {
      data,
      columns,
      showPagination,
      onPageChange,
      currentPage,
      isLastPage,
      pageSize,
      totalCount,
      isFetching,
      noDataComponent,
      onRowClick,
      onKeyDown,
    } = this.props;
    return (
      <div>
        <div className={styles.tableContainer}>
          <TableHeader columns={columns} />
          <TableData
            data={data}
            columns={columns}
            isFetching={isFetching}
            noDataComponent={noDataComponent}
            onRowClick={onRowClick}
            onKeyDown={onKeyDown}
          />
        </div>
        {showPagination && totalCount > 0 && (
          <Pagination
            currentPage={currentPage}
            pageSize={pageSize}
            isLastPage={
              isLastPage || totalCount <= (currentPage + 1) * pageSize
            }
            onPageChange={onPageChange}
            totalCount={totalCount}
          />
        )}
      </div>
    );
  }
}

Table.propTypes = {
  /** array of objects - object have same structure as the column accessors */
  data: arrayOf(instanceOf(Object)),
  /** array of objects - column (Table Header):
   *
   * structure - id, header, accessor, type
   * id - string, number
   * header - string, node
   * accessor - string (object key which data row will have)
   * type - col0, col0-1, col1, col2, col3, col4
   *
   * */
  columns: arrayOf(
    shape({
      id: oneOfType([PropTypes.string, PropTypes.number]),
      header: oneOfType([PropTypes.string, PropTypes.node]),
      accessor: PropTypes.string,
      type: PropTypes.string,
    })
  ),
  /** total count for pagination */
  totalCount: PropTypes.number,
  /** to show pagination or not */
  showPagination: PropTypes.bool,
  /** to show loader */
  isFetching: PropTypes.bool,
  /** pagination - if its last page or not */
  isLastPage: PropTypes.bool,
  /** page size - how many rows are supposed to be shown */
  pageSize: PropTypes.number,
  /** current page index - starts at 0 */
  currentPage: PropTypes.number,
  /** function fired when there is a change in page number */
  onPageChange: PropTypes.func,
  /** function fired when there is a user clicks on a row */
  onRowClick: PropTypes.func,
  /** on key down function for a table row */
  onKeyDown: PropTypes.func,
  /** if you want custom message/ component if there is no data */
  noDataComponent: oneOfType([PropTypes.node, PropTypes.string]),
};

Table.defaultProps = {
  data: [],
  columns: [],
  totalCount: 0,
  showPagination: false,
  isFetching: false,
  pageSize: 10,
  currentPage: 0,
  isLastPage: null,
  onPageChange: null,
  onRowClick: null,
  onKeyDown: null,
  noDataComponent: "No data to show.",
};

export default Table;
