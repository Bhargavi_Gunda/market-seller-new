import React from "react";

import PropTypes from "prop-types";
import cx from "../../utils/classNames";
import styles from "./Pagination.module.scss";

const Pagination = (props) => {
  const {
    pageSize = 10,
    onPageChange,
    currentPage = 0,
    totalCount = 0,
    initialClusterSize,
    showPageCount,
  } = props;
  const totalPage = Math.ceil(totalCount / pageSize);
  const isLastPage = currentPage === totalPage;
  if (totalPage <= 1) {
    return null;
  }

  if (initialClusterSize < 3) {
    return (
      <div>
        initialClusterSize is too small to render pls keep it greater than or
        equall to 3
      </div>
    );
  }
  if (initialClusterSize % 2 === 0) {
    console.warn("Use odd value of initialClusterSize to avoid flicker");
  }

  const onPageClick = (pageNo) => {
    if (!Number.isNaN(+pageNo) && pageNo !== currentPage) {
      onPageChange && onPageChange(pageNo);
    }
  };

  const renderPageNos = () => {
    const totalPositions = initialClusterSize + 2;
    const upperCut = totalPage - initialClusterSize + 1;
    const arr = [];
    if (totalPage <= totalPositions) {
      for (let i = 1; i <= totalPage; i++) {
        arr[i] = i;
      }
    } else {
      arr.push(1);
      if (currentPage < initialClusterSize) {
        for (let i = 2; i <= initialClusterSize; i++) {
          arr[i] = i;
        }
        arr.push("...");
      } else if (currentPage > upperCut) {
        arr.push("...");
        for (let i = upperCut; i < totalPage; i++) {
          arr[i] = i;
        }
      } else {
        let middleCut = totalPositions - 4;
        if (middleCut % 2 === 0) {
          middleCut -= 1;
        }
        arr.push("...");
        for (let i = Math.floor(middleCut / 2); i > 0; i--) {
          arr.push(currentPage - i);
        }
        arr.push(currentPage);
        for (let i = 1; i < Math.ceil(middleCut / 2); i++) {
          arr.push(currentPage + i);
        }
        arr.push("...");
      }
      arr.push(totalPage);
    }

    return arr.map((el) => (
      <span
        key={el}
        className={cx(styles.pageNo, {
          [styles.currentPage]: el === currentPage,
          [styles.dot]: el === "...",
        })}
        onClick={() => onPageClick(el)}
      >
        {el}
      </span>
    ));
  };

  return (
    <div
      className={cx(styles.pagination, {
        [styles.withoutPageCount]: !showPageCount,
      })}
    >
      {showPageCount && (
        <div className={styles.totelPage}>
          Page<span>{currentPage}</span>of<span>{totalPage}</span>
        </div>
      )}
      <div className={styles.prevNextArrows}>
        <button
          type="button"
          disabled={currentPage < 2}
          className={cx([styles.prev], { [styles.disabled]: currentPage < 2 })}
          onClick={() => {
            if (currentPage >= 2 && onPageChange) {
              onPageChange(currentPage - 1);
            }
          }}
        >
          <i className={styles.left} />
          Prev
        </button>
        {renderPageNos(totalPage)}
        <button
          type="button"
          disabled={isLastPage}
          className={cx([styles.next], { [styles.disabled]: isLastPage })}
          onClick={() => {
            if (!isLastPage && onPageChange) {
              onPageChange(currentPage + 1);
            }
          }}
        >
          Next
          <i className={styles.right} />
        </button>
      </div>
    </div>
  );
};

Pagination.propTypes = {
  pageSize: PropTypes.number,
  onPageChange: PropTypes.func,
  currentPage: PropTypes.number,
  totalCount: PropTypes.number,
  initialClusterSize: PropTypes.number,
};

Pagination.defaultProps = {
  pageSize: 1,
  onPageChange: null,
  currentPage: 0,
  totalCount: 0,
  initialClusterSize: 3,
};

export default Pagination;
