import React, { useEffect, useState } from "react";
import Table from "./Table";

const pageSize = 5;

const promiseTimeout = (time) => () =>
  new Promise((resolve) => setTimeout(resolve, time));

const chunking = (array, chunk) =>
  array.reduce((resultArray, item, index) => {
    const chunkIndex = Math.floor(index / chunk);
    if (!resultArray[chunkIndex]) {
      // eslint-disable-next-line
      resultArray[chunkIndex] = []; // start a new chunk
    }
    resultArray[chunkIndex].push(item);
    return resultArray;
  }, []);

const fetchData = [
  { id: 1, name: "Rajat", age: "24" },
  { id: 2, name: "Imam", age: "24" },
  { id: 3, name: "Deepak", age: "24" },
  { id: 4, name: "Tushar", age: "24" },
  { id: 5, name: "Dikshant", age: "24" },
  { id: 6, name: "Vipul", age: "24" },
  { id: 7, name: "Abhay", age: "24" },
  { id: 8, name: "Himanshu", age: "24" },
  { id: 9, name: "Kshitij", age: "24" },
  { id: 10, name: "Mudit", age: "24" },
  { id: 11, name: "Shyam", age: "24" },
  { id: 12, name: "Prateek", age: "24" },
  { id: 13, name: "Swati", age: "24" },
  { id: 14, name: "Gagandeep", age: "24" },
  { id: 15, name: "Kavita", age: "24" },
];

const TableContainer = () => {
  const [currentPage, setCurrentPage] = useState(0);
  const [data, setData] = useState([]);
  const [isFetching, setFetching] = useState(true);
  const handleCurrentPage = (page) => {
    if (currentPage <= 2) {
      setCurrentPage(page);
    }
  };
  const handleRowClick = (value) => {
    // eslint-disable-next-line
    console.log("row clicked: ", value);
  };
  useEffect(() => {
    // mock api
    const newPromise = promiseTimeout(3000);
    newPromise().then(() => {
      setFetching(false);
      const result = chunking(fetchData, pageSize);
      setData(result[0]);
    });
  }, []);

  useEffect(() => {
    const result = chunking(fetchData, pageSize);
    setData(result[currentPage]);
  }, [currentPage]);
  const columns = [
    { id: 1, header: "Name", accessor: "name" },
    { id: 2, header: "Age", accessor: "age", type: "col0-1" },
  ];
  return (
    <Table
      data={data}
      columns={columns}
      totalCount={fetchData.length}
      currentPage={currentPage}
      pageSize={pageSize}
      onPageChange={handleCurrentPage}
      showPagination
      isFetching={isFetching}
      onRowClick={handleRowClick}
    />
  );
};

export default TableContainer;
