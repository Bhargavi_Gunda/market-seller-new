import React, { memo } from "react";
import s from "./Spinner.module.scss";
import cx from "../../utils/classNames";

const Spinner = ({ fullpage }) => {
  return (
    <div className={cx({ [s.wrapper]: fullpage === true })}>
      <div>
        <div className={s.spinner}>
          <div className={s.bounce1}></div>
          <div className={s.bounce2}></div>
          <div className={s.bounce3}></div>
          <div className={s.bounce4}></div>
          <div className={s.bounce5}></div>
        </div>
      </div>
    </div>
  );
};

export default memo(Spinner);

Spinner.defaultProps = {
  fullpage: true,
};
