import React, { Component } from "react";
import PropTypes from "prop-types";

import styles from "./Input.module.scss";

class Input extends Component {
  handleChange = (evt) => {
    evt.persist();
    const {
      props: { onChange },
    } = this;
    const {
      target: { value },
    } = evt;
    if (onChange) onChange(value, evt);
  };

  render() {
    const {
      props: { label, errorMessage, placeholder },
    } = this;
    return (
      <div className={styles.floatingLabel}>
        <input
          type="text"
          placeholder={placeholder}
          onKeyUp={this.handleChange}
        />
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        {label && <label>{label}</label>}
        {errorMessage && <span className={styles.error}>{errorMessage}</span>}
      </div>
    );
  }
}

Input.propTypes = {
  /** your label name */
  label: PropTypes.string,
  /** your custom error message */
  errorMessage: PropTypes.string,
  /** input on change function */
  onChange: PropTypes.func,
  /** placeholder */
  placeholder: PropTypes.string,
};

Input.defaultProps = {
  label: "",
  onChange: () => {},
  errorMessage: "",
  placeholder: " ",
};

Input.displayName = "Input";

export default Input;
