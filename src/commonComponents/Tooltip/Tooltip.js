import React, { Component } from "react";

import PropTypes, {
  /* tree-shaking no-side-effects-when-called */ oneOfType,
} from "prop-types";
import cx from "../../utils/classNames";
import styles from "./Tooltip.module.scss";

class Tooltip extends Component {
  render() {
    const {
      props: { direction, children, parent },
    } = this;
    return (
      <div className={styles.tooltip} data-direction={direction}>
        <div className={cx(styles.tooltipInitiator)}>{parent}</div>
        <div className={styles.tooltipItem}>{children}</div>
      </div>
    );
  }
}

Tooltip.propTypes = {
  /** bottom | top | left | right */
  direction: PropTypes.string,
  /** react component that will be rendered tooltip content */
  children: oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  /** react component which is the parent of tooltip */
  parent: oneOfType([PropTypes.string, PropTypes.node]).isRequired,
};

Tooltip.defaultProps = {
  direction: "top",
};

export default Tooltip;
