import { combineReducers } from "redux";
import appReducer from "../store/appReducer";
import LeftPanelReducer from './LeftPanelReducer'

const rootReducer = (history) =>
  combineReducers({
    appReducer,
    LeftPanelReducer
  });

export default rootReducer;
