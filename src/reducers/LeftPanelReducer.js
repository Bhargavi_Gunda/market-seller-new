import * as Actions  from '../ActionConstants/LeftPanelConstants';

let inialState = {
    currentActiveTab : {}
}

export default function LeftPanelReducer( state = inialState, action ) {
    var currentState = state;
    switch(action.type) {
        case Actions.SET_LEFT_PANEL_DETAILS: {
            console.log("Active tab" ,action.tabDetails.link )
            currentState.currentActiveTab = action.tabDetails
            return currentState
        }
        case Actions.SET_ACTIVE_TAB:{
            // need to add logic 
        }
        default : {
            return currentState;
        }
    }
}