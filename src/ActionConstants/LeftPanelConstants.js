/**
 *  Left Panel Action Constants;
 */

export let SET_LEFT_PANEL_DETAILS= "SET_LEFT_PANEL_DETAILS";
export let  SET_USER_DATA = "SET_USER_DATA";
export let  SET_LEFT_PANEL_DATA = "SET_LEFT_PANEL_DATA";
export let  SET_HEADER_TITLE = "SET_HEADER_TITLE";
export let SET_ACTIVE_TAB = "SET_ACTIVE_TAB";