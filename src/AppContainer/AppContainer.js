import React, { Component } from "react";
import Routers from "../routes/routes";
// import AppLayout from '../AppLayout/AppLayout'
import DefaultLayout from "../DefaultLayout/DefaultLayout";

 export default class AppContainer extends Component {
  render() {
    return (
      <div className={"notch"}>
        <DefaultLayout>
          <Routers />
        </DefaultLayout>
      </div>
    );
  }
}
AppContainer.propTypes = {};
AppContainer.defaultProps = {};
