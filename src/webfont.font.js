module.exports = {
  files: ["../public/svg/*.svg"],
  fontName: "fontIcons",
  classPrefix: "wf-",
  baseSelector: ".fontIcons",
  types: ["eot", "woff", "woff2", "ttf"],
  fileName: "static/fonts/roboto/webfonts.[fontname].[hash].[ext]",
};
