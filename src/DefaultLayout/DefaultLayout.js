import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Header from "../commonComponents/Seller/Header/Header";
// import at from "lodash/at";
// import get from 'lodash/get'

class DefaultLayout extends Component {
  render() {
    const { leftPanelData, children } = this.props;
    return (
      <main>
        <header ref={this.headerRef}>
          <Header
            leftPanelData={leftPanelData}
            userData={this.props.userInfo}
          />
        </header>
        {children}
      </main>
    );
  }
}

DefaultLayout.propTypes = {
  leftPanelData: PropTypes.arrayOf(PropTypes.any).isRequired,
  userInfo: PropTypes.objectOf(PropTypes.any).isRequired,
};
DefaultLayout.defaultProps = {};

const mapStateToProps = (state) => ({
  leftPanelData: state.appReducer.leftPanelData,
  userInfo: state.appReducer.userInfo || {}
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);
