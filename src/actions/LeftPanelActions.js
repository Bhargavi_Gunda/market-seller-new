import  * as Actions from '../ActionConstants/LeftPanelConstants';

export function setActiveTabDetails(tabDetails) {
 return {
     type : Actions.SET_LEFT_PANEL_DETAILS,
     tabDetails
 }};

export const setLeftPanelData = (payload) => ({
  type: Actions.SET_LEFT_PANEL_DATA,
  payload,
});

// Tab data 
export const sessionObjActionCreator = (data) => (
  {
  type: Actions.SET_USER_DATA,
  payload: data,
});

export const setUserData = (payload) => (
  { 
  type: Actions.SET_USER_DATA, payload });

export const setHeaderTitle = (payload) => ({
  type: Actions.SET_HEADER_TITLE,
  payload,
});

export function setSelectedTab(tabInfo) {
    return{
        type : Actions.SET_ACTIVE_TAB,
        payload : tabInfo
    }
}
