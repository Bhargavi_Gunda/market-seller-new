import { fromPairs } from "ramda";
import { actionTypes } from "../constants";
import * as Actions from '../ActionConstants/LeftPanelConstants'

const initialState = {
  userInfo: null,
  leftPanelData: null,
  headerTitle: "",
};

export default function appReducer(state = initialState, { type, payload }) {
  switch (type) {
    case Actions.SET_USER_DATA:
      return { ...state, userInfo: payload };
    case Actions.SET_LEFT_PANEL_DATA:
      return { ...state, leftPanelData: payload };
    case Actions.SET_HEADER_TITLE:
      return { ...state, headerTitle: payload };
    default:
      return state;
  }
}
