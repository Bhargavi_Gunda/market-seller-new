export const disableBodyScrolling = (flag) => {
  if (flag) {
    document.body.style.position = "fixed";
  } else {
    document.body.style.position = "";
  }
};

export const bodyHack = (flag) => {
  if (flag) {
    document.body.style.overflow = "hidden";
  } else {
    document.body.style.overflow = "visible";
  }
};

export const goToBackScreen = () => {
  try {
    window.history.back();
  } catch (e) {
    // Error handling here
  }
};

export const disableBgScrolling = () => {
  disableBodyScrolling(true);
  bodyHack(true);
  window.setTimeout(() => {
    bodyHack(true);
  }, 0);
};

export const enableBgScrolling = () => {
  disableBodyScrolling(false);
  bodyHack(false);
  window.setTimeout(() => {
    bodyHack(false);
  }, 0);
};
