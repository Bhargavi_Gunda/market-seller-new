import React from "react";
import { useToaster } from "@paytm-common/alerts";

export const withToasterHook = (Component) => {
  return (props) => {
    const toaster = useToaster();

    return <Component toaster={toaster} {...props} />;
  };
};

export function getDebouncedFunction(callback, timeDelay) {
  let timerId;
  return function (...args) {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      callback(...args);
    }, timeDelay);
  };
}

const MULTI_CLICK_DELAY = 500;

let timerId = null;
export function preventMultiClick(callback, ...args) {
  if (!timerId) {
    callback(...args);
    timerId = setTimeout(() => {
      timerId = null;
    }, MULTI_CLICK_DELAY);
  }
}
