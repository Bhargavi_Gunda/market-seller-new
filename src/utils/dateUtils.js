import moment from "moment";
import { MONTHS_ARRAY_SHORT } from "../constants/dateConstants";

export function getDateAfterDays(date = new Date(), nDays, nMonths) {
  let currentDate = new Date(date.getTime());
  if (nDays) {
    currentDate.setDate(currentDate.getDate() + nDays);
  } else if (nMonths) {
    currentDate.setMonth(currentDate.getMonth() + nMonths);
  }
  return currentDate;
}

// Convert 26/12/2013 to 12/26/2013 and then 6 Dec, 2013
export const convertToStrFormat = (date) => {
  let str = date;
  if (str.includes("/")) {
    let dateArr = str.split("/");
    str = `${dateArr[1]}/${dateArr[0]}/${dateArr[2]}`;
  }
  let dateObj = new Date(str);
  return `${dateObj.getDate()} ${
    MONTHS_ARRAY_SHORT[dateObj.getMonth()]
  }, ${dateObj.getFullYear()}`;
};

export const convertToFormat = (date, format = "mm-dd-yyyy", nappendZero) => {
  let month = "" + (date.getMonth() + 1);
  let day = "" + date.getDate();
  let year = date.getFullYear();

  if (!nappendZero) {
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
  }

  let finalDate;
  const separator = format.indexOf("/") > -1 ? "/" : "-";
  switch (format.toLowerCase()) {
    case "mm-dd-yyyy" || "mm/dd/yyyy":
      finalDate = [month, day, year].join(separator);
      break;
    case "dd-mm-yyyy" || "dd/mm/yyyy":
      finalDate = [day, month, year].join(separator);
      break;
    case "yyyy-mm-dd" || "yyyy/mm/dd":
      finalDate = [year, month, day].join(separator);
      break;
    case "yyyy/dd/mm" || "yyyy-dd-mm":
      finalDate = [year, day, month].join(separator);
      break;
    case "ddmmm":
      finalDate = `${date.getDate()} ${MONTHS_ARRAY_SHORT[date.getMonth()]}`;
      break;
    default:
      finalDate = [day, month, year].join(separator);
  }

  return finalDate;
};

export const daysBetween = (date1, date2) => {
  // The number of milliseconds in one day
  let ONE_DAY = 1000 * 60 * 60 * 24;

  // Convert both dates to milliseconds and set to midnight
  let date1Obj = new Date(date1).setHours(0, 0, 0, 0);
  let date2Obj = new Date(date2).setHours(0, 0, 0, 0);

  // Calculate the difference in milliseconds
  let diff = Math.round(date2Obj - date1Obj);

  // Convert back to days and return
  return Math.ceil(diff / ONE_DAY);
};

export const getDateFromString = (dateStr, format = "dd/mm/yyyy") => {
  if (typeof dateStr === "string" && dateStr.length > 0) {
    let day, month, year;
    const separator = format.indexOf("/") > -1 ? "/" : "-";
    switch (format.toLowerCase()) {
      case "dd-mm-yyyy":
      case "dd/mm/yyyy":
        [day, month, year] = dateStr.split(separator);
        break;
      default:
        break;
    }
    // standard format : YYYY-MM-DD
    return new Date([year, month, day].join("-"));
  }
};

export const getTimeinAmPm = (timeString) => {
  //input: 00:00:00
  var hourEnd = timeString.indexOf(":");
  var H = +timeString.substr(0, hourEnd);
  var h = H % 12 || 12;
  var ampm = H < 12 || H === 24 ? " AM" : " PM";
  return h + timeString.substr(hourEnd, 3) + ampm;
};

export const formatDate = (
  date,
  isMonthStr = false,
  isTimeDisplay = false,
  isTimeOnly = false
) => {
  if (typeof date === "string" && date.indexOf("+") >= 0)
    date = date.split("+")[0];
  let dateObj = new Date(date);
  if (!isNaN(dateObj.getMonth())) {
    let day = dateObj.getDate().toString();
    let month = (dateObj.getMonth() + 1).toString();
    day = day.length < 2 ? "0" + day : day;
    month = month.length < 2 ? "0" + month : month;
    let monthStr = MONTHS_ARRAY_SHORT[dateObj.getMonth()];
    let returnDate;
    const addZero = (time) => {
      return time.toString().length < 2 ? "0" + time.toString() : time;
    };
    if (isMonthStr) {
      returnDate = `${day} ${monthStr} ${dateObj.getFullYear()}`;
    } else if (isTimeDisplay) {
      returnDate = `${dateObj.getFullYear()}-${month}-${day} ${addZero(
        dateObj.getHours()
      )}:${addZero(dateObj.getMinutes())}:${addZero(dateObj.getSeconds())}`;
    } else if (isTimeOnly) {
      return (
        `${day} ${monthStr} ${dateObj.getFullYear()} ` +
        getTimeinAmPm(
          `${addZero(dateObj.getHours())}:${addZero(
            dateObj.getMinutes()
          )}:${addZero(dateObj.getSeconds())}`
        )
      );
    } else {
      returnDate = `${day}-${month}-${dateObj.getFullYear()}`;
    }
    return returnDate;
  } else {
    return "";
  }
};

//'format 5 Jul 1995 to 5/7/1995'
export const formatDateString = (date) => {
  const arr = date.split(" ");
  const month = MONTHS_ARRAY_SHORT.indexOf(arr[1]) + 1;
  return `${arr[0]}/${month}/${arr[2]}`;
};

//'format 1992-10-14  to 14 Oct 1992'
export const formatStringDate = (date) => {
  if (date) {
    const arr = date.split("-");
    const month = MONTHS_ARRAY_SHORT[parseInt(arr[1]) - 1];
    return `${arr[2]} ${month} ${arr[0]}`;
  }
  return "";
};

export const getCurrentAgeInYears = (dob) => {
  return moment().diff(moment(dob, "DD/MM/YYYY"), "years", false);
};

// // 2019-07-26 15:55:44.0 to
// export const formatDateTime=(date)=>{
//     const arr = date.split(' ');
//     const
// }

// Get Date-Object from any date format String
// (dd-mm-yyyy || dd/mm/yyyy or the string which can give Valid Date object), Support of (-, /, aug)
export const getDateObject = (date) => {
  let str = date;
  let checkItem = (item) => (item.length === 1 ? "0" + item : item);
  if (!str) {
    return null;
  }
  if (str.includes("/")) {
    let dateArr = str.trim().split("/");
    let newDateArr = dateArr.map((item) => checkItem(item));
    str = newDateArr.reverse().join("-");
  } else if (str.includes("-")) {
    let dateArr = str.trim().split("-");
    let newDateArr = dateArr.map((item) => checkItem(item));
    str = newDateArr.reverse().join("-");
  }
  return new Date(str);
};

export const dateConvertHandler = (date) => {
  let newDate = new Date(date.replace("IST", ""));
  if (isNaN(newDate.getTime())) {
    newDate = new Date(date.replace(/ /g, "T"));
  }
  return newDate;
};

export const time_ago = (inputDate) => {
  const ONE_MINT = 60 * 1000;
  const ONE_HOUR = 60 * 60 * 1000;
  // const ONE_DAY = 24 * 60 * 60 * 1000;
  const dateNow = new Date();
  const seconds = dateNow - inputDate;
  if (daysBetween(inputDate, dateNow) === 0) {
    if (seconds < ONE_MINT) {
      return `Less than 1 minute ago`;
    } else if (seconds < ONE_HOUR) {
      return `${Math.floor(seconds / ONE_MINT)} minute ago`;
    } else {
      return `${inputDate.toLocaleString("en-US", {
        hour: "numeric",
        minute: "numeric",
        hour12: true,
      })}`;
    }
  } else {
    return formatDate(inputDate, true);
  }
};
// gets 1st, 2nd, 3rd from date from date object
// gets 1,2,3 month from date object
// gets full year
export const getDateElement = (
  dateObject,
  date = false,
  month = false,
  year = false
) => {
  if (dateObject instanceof Date && !isNaN(dateObject)) {
    if (date)
      return ((d) => {
        if (d > 3 && d < 21) return d + "th";
        switch (d % 10) {
          case 1:
            return d + "st";
          case 2:
            return d + "nd";
          case 3:
            return d + "rd";
          default:
            return d + "th";
        }
      })(dateObject.getDate());
    else if (month) return dateObject.getMonth() + 1;
    else return dateObject.getFullYear();
  }
  return "NA";
};
