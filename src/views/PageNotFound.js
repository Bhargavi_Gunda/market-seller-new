import React, { PureComponent } from "react";

const divStyle = {
  margin: "40px",
  backgroundColor: "red",
  padding: "56px 24px",
  borderRadius: "6px",
  fontSize: "32px",
};
class PageNotFound extends PureComponent {
  render() {
    return <div style={divStyle}>404 - Page Not Found !</div>;
  }
}

export default PageNotFound;
