import React, { PureComponent } from "react";
import Spinner from "../commonComponents/Spinner/Spinner";

const SuccessWrapper = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  height: "80vh",
};

const text = {
  textAlign: "center",
  fontSize: "14px",
  color: "#333",
  marginTop: "40px",
  padding: "10px 20px",
  lineHeight: "20px",
};

class Success extends PureComponent {
  render() {
    return (
      <>
        <div style={text}>
          Please do not go back or refresh your page. We are fetching your
          details...
        </div>
        <div style={SuccessWrapper}>
          <Spinner fullpage={false} />
        </div>
      </>
    );
  }
}

export default Success;
