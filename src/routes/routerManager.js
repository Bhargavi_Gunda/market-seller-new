import * as R from "ramda";

const routes = {
  growth: {
    path: "/growth",
    name: "growth",
  },
  loans : {
    path: "/loans",
    name: "loans",
  }
};
export default routes;
export const getPathByKey = (key) => routes[key].path;
export const getPathByName = (name) => {
  const findPathByName = R.compose(
    R.prop("path"),
    R.find(R.propEq("name", name)),
    R.values
  );
  return findPathByName(routes);
};
