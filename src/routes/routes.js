import React, { Component } from "react";
import { Router , Switch } from "@reach/router";
// import Loadable from "react-loadable";
import { onRouteChangeEvent } from "./onRoutesChange";
import { getPathByName } from "./routerManager";
// import Spinner from "../commonComponents/Spinner";
import loadable from '@loadable/component'
import Loans from "../components/Loans/Loans";


// const LoadingFallback = (props) => {
//   if (props.error) {
//     console.log(props.error);
//   }

//   if (props.children) {
//     console.log("Loading Children");
//     return <React.Fragment>{props.children}</React.Fragment>;
//   }

//   return <Spinner />;
// };

// const AsyncPageNotFound = Loadable({
//   loader: () =>
//     import(/* webpackChunkName: "PageNotFound" */ "../views/PageNotFound"),
//   // eslint-disable-next-line react/display-name
//   loading: (error) => <LoadingFallback {...error} />,
//   modules: ["PageNotFound"],
// });

// const AsyncGrowthComponentLoad = Loadable({
//   loader: () =>
//     import(/* webpackChunkName: "Growth" */ "../components/Growth/Growth"),
//   // eslint-disable-next-line react/display-name
//   loading: (error) => <LoadingFallback {...error} />,
//   modules: ["growth"],
// });

const AsyncGrowthComponentLoad = loadable(() => import('../components/Growth/Growth'));
const AsyncLoansComponentLoad = loadable(() => import('../components/Loans/Loans'));
// const AsyncPromotionComponentLoad = loadable(() => import('../components/Promotion/Promotion'));


export const routes = [
  {
    path: getPathByName("growth"),
    component: AsyncGrowthComponentLoad,
    exact: true,
  },
  {
    path: getPathByName("loans"),
    component: AsyncLoansComponentLoad,
    exact: true,
  }
];


export default class Routers extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      onRouteChangeEvent("data from route changes");
    }
  }

  render() {
    return (
      <React.Fragment>
        <Router>
          <AsyncGrowthComponentLoad path="/growth"/>
          <AsyncGrowthComponentLoad path="loans/*"/>
        </Router>
      </React.Fragment>
    );
  }
}
