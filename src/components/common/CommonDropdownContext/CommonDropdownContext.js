import React, { Component } from 'react';
// import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
import * as s from './CommonDropdownContext.module.scss';
import { disableBodyScrolling } from '../../../services/common/coreUtilsHomePage';

class CommonDropdownContext extends Component {
  constructor(props) {
    super(props);
    this.wrapperRef = null;
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  handleClickOutside(event) {
    const { onOutsideClick } = this.props;
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      onOutsideClick();
    }
  }

  componentDidMount() {
    disableBodyScrolling(true);
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    disableBodyScrolling(false);
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  render() {
    const { children, label, rightAlignment = false } = this.props;
    return (
      <div className={cx(s.dropdown, s.open)}>
        {label && <div className={s.dLabel}>{label}</div>}
        <div
          ref={this.setWrapperRef}
          className={
            rightAlignment ? s.insiderDropdownMenu : s.headerDropdownMenu
          }
        >
          <div>{children}</div>
        </div>
        <div className={s.backdrop} />
      </div>
    );
  }
}

export default CommonDropdownContext;
