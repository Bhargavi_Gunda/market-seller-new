import React from 'react'

export default function LoanNavigationPanel() {
    return (
        <div style={{
            display:'flex',
            direction:'row'
        }}>
            <nav>
                <ul>
                    <li> Loan Approve </li>
                    <li> Loan Status </li>
                    <li> Loan Apply </li>
                </ul>
            </nav>
        </div>
    )
}
