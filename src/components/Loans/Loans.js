import React from 'react'
import LoanNavigationPanel from './LoanNavigationPanel'
import { Router , Link} from '@reach/router'
import LoanApply from './LoanApply'
import LoandApprove from './LoandApprove'
import LoanStatus from './LoanStatus'
import { HomeLoan, navigate } from './HomeLoan'


const Dashboard = () => (
    <div>
      <h2>Dashboard</h2>
      <nav>
        <Link to="./">Loan home</Link> - {" "}
        <Link to="apply">Loan Apply</Link> - {" "}
        <Link to="status/:id">Laon staus</Link>
      </nav>
  
      <Router>
        <HomeLoan path="/" />
        <LoanApply path="apply" />
        <LoanStatus path="status/:id" />
    </Router>
    </div>
  );


export default function Loans() {

    return(
        <>
        <LoanNavigationPanel/>
        <Dashboard/>
</>
    )
}
