server {
        add_header Content-Security-Policy-Report-Only "script-src 'self' 'unsafe-inline' *.googletagmanager.com *.google-analytics.com; font-src 'self' *.gstatic.com;report-uri https://csp-report.mypaytm.com/reportcspviolations.php";
        server_name dslr-staging.paytm.com;
        ssl_protocols TLSv1.2;
        ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
        ssl_prefer_server_ciphers on;
        location / {
                proxy_pass http://127.0.0.1:7777;
                proxy_connect_timeout 300;
                proxy_send_timeout 300;
                proxy_read_timeout 300;
                send_timeout 300;
                proxy_set_header HOST $server_name;
                access_log /var/log/nginx/dslr.access.log;
                error_log /var/log/nginx/dslr.access.log;
        }

}