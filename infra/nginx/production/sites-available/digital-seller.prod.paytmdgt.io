# ngx_lua settings
#lua_package_path '$prefix/lua/?.lua;;';

server {
        server_name dslr.paytmdgt.io dslr-beta.paytmdgt.io;
       location / {
                proxy_pass http://127.0.0.1:7777;
                log_by_lua_file lua/api_latency.lua;
                proxy_connect_timeout 300;
                proxy_send_timeout 300;
                proxy_read_timeout 300;
                send_timeout 300;
                proxy_set_header HOST $server_name;
                access_log /var/log/nginx/dslr.access.log;
                error_log /var/log/nginx/dslr.error.log;
       }
}